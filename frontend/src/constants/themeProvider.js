import { ThemeProvider } from 'styled-components'
import React from 'react'

const theme = {
  colours: {
    primaryBack: '#1b181b',
    primaryBackLight: '#232123',

    primaryText: '#ffffff',

    gray: '#969696',
    green: '#28a745',
    red: '#aa4f43',
    orange: '#c0702f',

    borderInput: '#424242',
    focusInput: '#ffc106',

    secondary: '#658bf5',
    secondaryLight: '#8db0ff',
    backgroundPrimary: '#ffffff',
    backgroundSecondary: '#f5f1ee',
    primaryGradientLight: 'rgba(255, 199, 93, 1.0)',
    primaryGradientDark: 'rgba(255, 163, 99, 1.0)',
    primaryGradientLightDisabled: 'rgba(255, 199, 93, 0.6)',
    primaryGradientDarkDisabled: 'rgba(255, 163, 99, 0.6)',
    black: '#202020',
    blackLight: '#232123',
    white: '#ffffff',
    greyDisabled: '#cccccc',
    mainBackground: '#ffffff',

    darkGradient: 'rgba(7, 17, 32, 1.0)',
    lightGradient: 'rgba(18, 34, 64, 1.0)',
    mainBlue: 'rgba(39, 174, 234, 1.0)',
    mainDarkBlue: 'rgba(10, 48, 81, 1.0)',
    mainGreen: 'rgba(90, 235, 103, 1.0)',
    mainPurple: 'rgba(153, 70, 218, 1.0)',
    mainRed: 'rgba(223, 45, 66, 1.0)',
    mainIcon: 'rgba(108, 118, 144, 1.0)',
    navText: '#6c7690',
  },
  fontSizes: {
    '10px': '0.71rem',
    '11px': '0.78rem',
    '12px': '0.85rem',
    '13px': '0.92rem',
    '14px': '1rem',
    '15px': '1.07rem',
    '16px': '1.14rem',
    '17px': '1.21rem',
    '18px': '1.28rem',
    '19px': '1.35rem',
    '20px': '1.42rem',
    '21px': '1.5rem',
    '22px': '1.57rem',
    '32px': '2rem',
    '42px': '3rem',
    '60px': '3.75rem',
  },
  shadows: {
    primary: '2px 5px 10px -6px #f76b1c',
  },
}

export default props => <ThemeProvider theme={theme} {...props} />
