export const ROUTES = {
  LANDING: '/landing',
  MAIN: '/main',
}

export const ROUTE_TITLES = {
  LANDING: 'Landing',
  MAIN: 'Main',
}
