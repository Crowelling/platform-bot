export const russianTranslations = {
  lang_label: 'Russian',
  lang_key: 'ru',
  ERROR: {
    ERROR: 'Ошибка',
    NO_SUCH_USER: 'Пользователь не найден. Проверьте правильность ключа!',
    KEYS_NOT_EXIST: 'Не существует таких ключей для данной биржи!',
    SAME_EXCHANGE_ACCOUNT:
      'Вы не можете использовать один аккаунт для разных роботов.',
    WRONG_ROBOT_ID: 'Неверный ID бота.',
    INVALID_BALANCE:
      'Ваш баланс неправильный для данного тарифа, пожалуйста убедитесь в лимитах.',
    ADD_KEYS:
      'Возникли проблемы с настройкой робота, пожалуйста, обратитесь в техническую поддержку.',
    INVALID_BALANCE_CURRENCY: 'Недостаточно USD средств на балансе биржи.',
  },
  SUCCESS: {
    SUCCESS: 'Успех!',
    ADD_KEYS:
      'Ключи успешно добавлены! Идёт настройка робота. Это может занять до 24 часов.',
  },
  LANDING: {
    GET_STARTED: 'Начать',
    GET_KEY: 'Получить ключ',
    YOUR_KEY: 'Ваш ключ...',
    BANNER_EXPLAIN:
      ' - это торговые роботы нового поколения, алгоритмы которых разрабатывались под современные реалии торгового рынка криптовалют.',
    BANNER_GET_ROBOT: 'Подключите вашего торгового робота ',
    BANNER_TWO_CLICK: ' за 2 клика и начните получать прибыль уже сегодня.',
    CENTER_TRADE:
      'Торгуйте криптовалютами на ведущих биржах мира с помощью торговых роботов нового поколения ',
    CENTER_50: ' c доходностью от 50% даже без опыта в торговле.',
    BOTTOM_SECURITY:
      'Торговля осуществляется на ваших счетах. Ваши средства защищены от взлома. Вы самостоятельно выбираете доходность.',
    BOTTOM_FROM_MOMENT: 'С момента подключения ',
    BOTTOM_TILL_FEE:
      ' робота до первой прибыли всего 1 час. Прибыль получаете в Bitcoin!',
  },
  MAIN: {
    TOTAL_GET: 'Доход всего',
    GET_HALF_MONTH: 'Доход за 10 дней',
    TOTAL_SELLS: 'Всего продано',
    TOTAL_BUYS: 'Всего куплено',
    ACTIVE_ROBOTS: 'Активные роботы',
    HISTORY_ROBOTS: 'История роботов',

    EXCHANGE: 'Биржа',
    TARIFF: 'Тариф',
    DAY_START: 'Дата старта',
    DEPOSIT: 'Депозит',
  },
  TARIFF: {
    DETAIL: 'Подробнее',
    ACTIVATE: 'Активировать',
    WAIT: 'В обработке',
  },
  EXCHANGE: {
    WAITING:
      'Подождите немного, искусственный интеллект подбирает лучшие настройки!',
    CONNECT: 'Подключить',
  },
  FOOTER: {
    NARFEX_SUPPORT: 'При поддержке ',
  },

  HEADER: {
    FEATURES: 'Функции',
    TARIFFS: 'Тарифы',
    MARKETPLACE: 'Marketplace',
    RESOURCES: 'Банк знаний',
    ABOUT: 'О нас',
    LOG_IN: 'Вход',
    REGISTRATION: 'Регистрация',
  },
  NAVBAR: {
    HOME: 'Главная',
    PROFILE: 'Профиль',
    WALLET: 'Кошелек',
    TARIFF: 'Тариф',
    ROBOT: 'Подключение робота',
    PARTNERS: 'Партнеры',
    FAQ: 'FAQ Вопрос-ответ',
    LOGOUT: 'Выход',
    REFS: 'Реферальная система',
  },
  SIGN_UP: {
    REGISTRATION: 'Регистрация',
    EMAIL: 'Email адрес',
    PASSWORD: 'Пароль',
    CONFIRM_PASSWORD: 'Повторите пароль',
    REFERENCE_ID: 'ID пригласителя',
    TERMS_AND_CONDITIONALS: 'Согласен с условиями договора',
    NEXT: 'Далее',
    ALREADY_SIGNED: 'Я уже зарегестрирован',
    LOG_IN: 'Вход',
  },
  SIGN_IN: {
    ENTER_PRIVATE_CABINET: 'Вход в личный кабинет',
    EMAIL: 'Email адрес',
    PASSWORD: 'Пароль',
    LOG_IN: 'Вход',
    REGISTRATION: 'Регистрация',
  },
  MAIN_HEADER: {
    DAILY: '24 часа',
    WEEKLY: '7 дней',
  },
}

export const englishTranslations = {
  lang_label: 'English',
  lang_key: 'en',
  ERROR: {
    ERROR: 'Error',
    NO_SUCH_USER: 'User not found. Please check token valid.',
    KEYS_NOT_EXIST: 'The provided keys does not exist for current exchange.',
    SAME_EXCHANGE_ACCOUNT:
      "You can't use keys from Exchange account, which you are already used with another tariff.",
    WRONG_ROBOT_ID: 'Wrong robotID.',
    INVALID_BALANCE: 'Your balance is invalid for this tariff.',
    INVALID_BALANCE_CURRENCY:
      'Deposit your exchange USD balance before get in.',
  },
  SUCCESS: {
    SUCCESS: 'Success',
    ADD_KEYS:
      'API keys was successfully added. Bots are setting up. It might takes approximately 24 hours.',
  },
  LANDING: {
    GET_STARTED: 'Start',
    GET_KEY: 'Get Key',
    YOUR_KEY: 'Your key...',
    BANNER_EXPLAIN:
      ' - this is absolutely new generation trading robots which algorithms were developed and skillfully tuned for the nowadays realities of the cryptocurrency trading market.',
    BANNER_GET_ROBOT: 'Please connect your trading bot ',
    BANNER_TWO_CLICK: ' the profit is two clicks away.',
    CENTER_TRADE:
      "Trade cryptocurrencies on the world's leading exchanges using innovative trading robots ",
    CENTER_50: ' with a yield of 50% even without experience in trading.',
    BOTTOM_SECURITY:
      'Trading is on your accounts. Your funds are protected from hacking. You independently choose profitability.',
    BOTTOM_FROM_MOMENT: 'From the connection moment ',
    BOTTOM_TILL_FEE:
      ' there is one hour away to get your first profit. The income you getting in BTC!',
  },
  MAIN: {
    TOTAL_GET: 'Total profit',
    GET_HALF_MONTH: 'Profit for 10 days',
    TOTAL_SELLS: 'Total sells',
    TOTAL_BUYS: 'Total buys',
    ACTIVE_ROBOTS: 'Active bots',
    HISTORY_ROBOTS: 'Bots history',

    EXCHANGE: 'Exchange',
    TARIFF: 'Tariff',
    DAY_START: 'Day start',
    DEPOSIT: 'Deposit',
  },
  TARIFF: {
    DETAIL: 'Detail',
    ACTIVATE: 'Activate',
    WAIT: 'Loading',
  },
  EXCHANGE: {
    WAITING:
      'Just wait a bit, our AI is searching for the best configurations!',
    CONNECT: 'Connect',
  },
  FOOTER: {
    NARFEX_SUPPORT: 'Supported by ',
  },
}
