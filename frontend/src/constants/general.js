export const STATE = {
  IDDLE: 'iddle',
  REQUEST: 'request',
  SUCCESS: 'success',
  FAIL: 'fail',
}

export const TOKEN = 'token'
