import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Switch, Route, withRouter } from 'react-router-dom'

import MainContainer from '../MainContainer'
import LandingContainer from '../LandingContainer'

import { Splash, LoadingOverlay } from '../../components'
import { getItemAsync } from '../../redux/storage'
import { TOKEN } from '../../constants'
import { logout } from '../../redux/user/actions'

class App extends Component {
  async componentDidMount() {
    const isLogged = await getItemAsync(TOKEN)
    if (!isLogged) this.props.logout()
  }

  render() {
    return (
      <div>
        <LoadingOverlay />

        <Switch>
          <Route path="/" exact component={Splash} />
          <Route path="/landing" exact component={LandingContainer} />
          <Route path="/" component={MainContainer} />
        </Switch>
      </div>
    )
  }
}

const actions = {
  logout,
}

export default withRouter(
  connect(
    null,
    actions,
  )(App),
)
