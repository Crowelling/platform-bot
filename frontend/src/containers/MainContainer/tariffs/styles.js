import styled from 'styled-components'

export const CollapseContainer = styled.div`
  width: 100%;
  padding: 1em;
  border-radius: 2px;
  margin-top: 2em;
  box-shadow: 0 1px 15px rgba(0, 0, 0, 0.1), 0 1px 4px rgba(0, 0, 0, 1);
`

export const CollapseBody = styled.div`
  max-height: ${({ isOpen }) => (isOpen ? '100%' : '0')};
  overflow: hidden;
  transition: all 0.3s;
`

export const CollapseHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  div {
    flex: 1;
    text-align: center;
  }
`

export const HeaderData = styled.div`
  flex: 4 !important;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex: 0 0 5em;
  flex-wrap: wrap;
`

export const ExchangeContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin-top: 2em;
`

export const GearsImg = styled.img`
  height: 10em;
  width: 10em;
  margin: 2em;
`

export const WaitingContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 2em;
  text-align: center;
  align-items: center;
  justify-content: center;
`

export const InfoRow = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  flex-wrap: wrap;
`

export const AdaptiveTariffHeader = styled.div`
  width: 100%;
  display: flex;
`

export const DesktopTariffHeader = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  text-align: center;
`

export const TabletTariffHeader = styled.div``

export const TabletInfo = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 2em;
  text-align: center;
`

export const TabletButton = styled.div`
  display: flex;
  width: 100%;
  justify-content: end;
  justify-content: flex-end;
  align-items: flex-end;
`

export const MobileTariffHeader = styled.div`
  display: flex;
  flex-direction: column;
`

export const MobileExchange = styled.div`
  align-items: center;
  justify-content: center;
  display: flex;
  margin-top: 1em;
`

export const MobileInfo = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 2em;
  margin-top: 1em;
  div {
    margin-top: 1em;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
      div:nth-child(2) {
          text-align: end;
          display: flex;
          justify-content: flex-end;
       }  
    }
  }
`

export const MobileButton = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  margin-top: 1em;
`

export const selectStyles = {
  container: base => ({
    ...base,
    height: '42px',
    marginBottom: '1.3em',
    marginTop: '1em',
    width: '50%',
  }),
  control: () => ({
    minHeight: '42px',
    backgroundColor: 'trasparent',
    border: '1px solid #c0702f',
    display: 'flex',
    color: '#969696',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '2px',
    cursor: 'pointer',
    fontSize: '1em',
    fontWeight: 'normal',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
  }),
  dropdownIndicator: () => ({
    padding: '0 15px',
    border: 'none',
    cursor: 'pointer',
    color: '#c0702f',
    display: 'flex',
    alignItems: 'center',
  }),
  singleValue: () => ({
    color: '#969696',
  }),
  input: base => ({
    ...base,
    color: '#969696 !important',
  }),
  placeholder: () => ({
    fontSize: '1em',
    fontWeight: 'normal',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    color: '#969696',
  }),
  valueContainer: base => ({
    ...base,
    padding: '5px 10px',
  }),
  menu: () => ({
    position: 'absolute',
    top: '100%',
    width: '100%',
    margin: 0,
    backgroundColor: '#232123',
    boxShadow: '0 4px 16px 0 rgba(0, 0, 0, 0.15)',
    borderRadius: '2px',
    zIndex: 1,
  }),
  option: (base, state) => ({
    padding: '1em',
    backgroundColor: state.isFocused ? '#202020' : 'trasparent',
    cursor: 'pointer',
    fontSize: '1em',
    color: '#969696',
    fontWeight: 'normal',
    fontStyle: 'normal',
    fontStretch: 'normal',
    lineHeight: 'normal',
    letterSpacing: 'normal',
    zIndex: 10,
  }),
  indicatorSeparator: () => ({}),
}

export const ExchangeLogo = styled.img`
  height: 2em;
  display: block;
  margin-top: 0.5em;
`

export const ExchangeLogoWrapper = styled.div`
  flex: 0 !important;
  margin-right: 1em;
`
