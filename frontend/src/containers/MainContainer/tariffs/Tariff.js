import React, { Component } from 'react'
import { injectIntl } from 'react-intl'

import Exchange from './Exchange'
import TariffHeader from './TariffHeader'

import { CollapseBody, CollapseHeader, CollapseContainer } from './styles'

class Tariff extends Component {
  state = {
    isOpen: false,
  }

  toggleCollapse = () => {
    this.setState({ isOpen: !this.state.isOpen })
  }

  render() {
    const { isOpen } = this.state
    const { tariff, setExchangeKeys } = this.props
    if (!tariff) return

    return (
      <CollapseContainer>
        <CollapseHeader onClick={this.toggleCollapse}>
          <TariffHeader tariff={tariff} />
        </CollapseHeader>
        <CollapseBody isOpen={isOpen}>
          <Exchange tariff={tariff} setExchangeKeys={setExchangeKeys} />
        </CollapseBody>
      </CollapseContainer>
    )
  }
}

export default injectIntl(Tariff)
