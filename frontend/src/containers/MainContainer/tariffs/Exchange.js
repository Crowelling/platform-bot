import React, { Component } from 'react'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import borderRadius from 'highcharts-border-radius'
import HighchartsMore from 'highcharts/highcharts-more'
import Select from 'react-select'
import { injectIntl } from 'react-intl'

import {
  GearsImg,
  WaitingContainer,
  ExchangeContainer,
  InfoRow,
  selectStyles,
} from './styles'
import { Title, CardInfo, MainInput, MainButton } from '../../../components'
import gears from '../../../assets/images/execution.png'

borderRadius(Highcharts)
HighchartsMore(Highcharts)

const options = [
  { value: 'binance', label: 'Binance' },
  { value: 'bitmex', label: 'Bitmex' },
]

class Exchange extends Component {
  state = {
    apiKey: '',
    secretKey: '',
    exchange: { value: 'binance', label: 'Binance' },
  }

  changeExchange = exchange => {
    this.setState({ exchange })
  }

  handleApiKeyChange = e => {
    this.setState({ apiKey: e.target.value })
  }

  handleSecretKeyChange = e => {
    this.setState({ secretKey: e.target.value })
  }

  handleSetExchangeKeys = () => {
    const { setExchangeKeys, tariff } = this.props
    const { apiKey, secretKey, exchange } = this.state

    let data = {
      exchange: exchange.value,
      robotID: tariff.id,
      apiKey,
      secretKey,
    }

    setExchangeKeys(data)
  }

  renderExchange = tariff => {
    if (tariff.status === 'active') {
      return this.renderActiveExchange(tariff.exchange)
    } else if (tariff.status === 'inactive') {
      return this.renderInactiveExchange()
    } else {
      return this.renderWaitingExchange()
    }
  }

  renderActiveExchange = exchange => {
    const { intl } = this.props
    const {
      graphicData,
      feeTotalUSD,
      feeHalfMonthUSD,
      totalCountSells,
      totalCountBuys,
    } = exchange

    return (
      <div>
        <HighchartsReact
          options={graphicData}
          highcharts={Highcharts}
          containerProps={{ style: { width: '100%' } }}
        />
        <InfoRow>
          <CardInfo
            amount={totalCountSells}
            title={intl.formatMessage({ id: 'MAIN.TOTAL_SELLS' })}
          />
          <CardInfo
            amount={totalCountBuys}
            title={intl.formatMessage({ id: 'MAIN.TOTAL_BUYS' })}
          />
          <CardInfo
            amount={`${feeHalfMonthUSD}$`}
            title={intl.formatMessage({ id: 'MAIN.GET_HALF_MONTH' })}
          />
          <CardInfo
            amount={`${feeTotalUSD}$`}
            title={intl.formatMessage({ id: 'MAIN.TOTAL_GET' })}
          />
        </InfoRow>
      </div>
    )
  }

  renderInactiveExchange = () => {
    const { intl } = this.props
    const { apiKey, secretKey, exchange } = this.state

    return (
      <div>
        <Select
          options={options}
          value={exchange}
          styles={selectStyles}
          onChange={this.changeExchange}
        />
        <MainInput
          top={1}
          value={apiKey}
          onChange={this.handleApiKeyChange}
          legend="API Key"
        />
        <MainInput
          top={1}
          bottom={1}
          value={secretKey}
          onChange={this.handleSecretKeyChange}
          legend="Secret Key"
        />
        <MainButton
          onClick={this.handleSetExchangeKeys}
          title={intl.formatMessage({ id: 'EXCHANGE.CONNECT' })}
        />
      </div>
    )
  }

  renderWaitingExchange = () => {
    const { intl } = this.props
    return (
      <WaitingContainer>
        <GearsImg src={gears} alt="waiting-gears" />
        <Title>{intl.formatMessage({ id: 'EXCHANGE.WAITING' })}</Title>
      </WaitingContainer>
    )
  }

  render() {
    return (
      <ExchangeContainer>
        {this.renderExchange(this.props.tariff)}
      </ExchangeContainer>
    )
  }
}

export default injectIntl(Exchange)
