import React, { Component } from 'react'

import {
  Desktop,
  Tablet,
  Mobile,
  Head,
  MainButton,
  Title,
} from '../../../components'
import logoBinance from '../../../assets/images/logoBinance.png'
import logoBitmex from '../../../assets/images/logoBitmex.png'

import {
  ExchangeLogo,
  HeaderData,
  AdaptiveTariffHeader,
  DesktopTariffHeader,
  TabletTariffHeader,
  MobileTariffHeader,
  TabletInfo,
  TabletButton,
  MobileExchange,
  MobileInfo,
  MobileButton,
  ExchangeLogoWrapper,
} from './styles'
import { injectIntl } from 'react-intl'

class TariffHeader extends Component {
  renderExchangeName = exchange => {
    if (!exchange)
      return (
        <ExchangeLogoWrapper>
          <Title style={{ minWidth: '104px' }}> - </Title>
        </ExchangeLogoWrapper>
      )

    let img = exchange.name === 'binance' ? logoBinance : logoBitmex
    return (
      <ExchangeLogoWrapper>
        <Desktop>
          <ExchangeLogo src={img} />
        </Desktop>
        <Tablet>
          <ExchangeLogo src={img} />
        </Tablet>
        <Mobile>
          <MobileExchange>
            <img src={img} alt="logoExchange" style={{ height: '3em' }} />
          </MobileExchange>
        </Mobile>
      </ExchangeLogoWrapper>
    )
  }

  render() {
    const { intl, tariff } = this.props
    const { name, status, exchange } = tariff

    let deposit, dayStart, feeTotalPercent, feeHalfMonthPercent

    if (tariff.exchange) {
      deposit = tariff.exchange.deposit
      dayStart = tariff.exchange.dayStart
      feeTotalPercent = tariff.exchange.feeTotalPercent
      feeHalfMonthPercent = tariff.exchange.feeHalfMonthPercent
    }

    return (
      <AdaptiveTariffHeader>
        <Desktop>
          <DesktopTariffHeader>
            {this.renderExchangeName(exchange)}
            <HeaderData>
              <Head>{name}</Head>
              <Head>{dayStart || '-'}</Head>
              <Head>{`${deposit || '-'} $`}</Head>
              <Head>{`${feeHalfMonthPercent || '-'} %`}</Head>
              <Head>{`${feeTotalPercent || '-'} %`}</Head>
            </HeaderData>
            <MainButton
              title={
                status === 'active'
                  ? intl.formatMessage({ id: 'TARIFF.DETAIL' })
                  : intl.formatMessage({ id: 'TARIFF.ACTIVATE' })
              }
              onClick={this.toggleCollapse}
              color={status === 'active' ? 'green' : 'orange'}
            />
          </DesktopTariffHeader>
        </Desktop>
        <Tablet>
          <TabletTariffHeader>
            <TabletInfo>
              {this.renderExchangeName(exchange)}
              <HeaderData>
                <Head>{name}</Head>
                <Head>{dayStart || '-'}</Head>
                <Head>{`${deposit || '-'} $`}</Head>
                <Head>{`${feeHalfMonthPercent || '-'} %`}</Head>
                <Head>{`${feeTotalPercent || '-'} %`}</Head>
              </HeaderData>
            </TabletInfo>
            <TabletButton>
              <MainButton
                title={
                  status === 'active'
                    ? intl.formatMessage({ id: 'TARIFF.DETAIL' })
                    : intl.formatMessage({ id: 'TARIFF.ACTIVATE' })
                }
                onClick={this.toggleCollapse}
                color={status === 'active' ? 'green' : 'orange'}
              />
            </TabletButton>
          </TabletTariffHeader>
        </Tablet>
        <Mobile>
          <MobileTariffHeader>
            <MobileExchange>{this.renderExchangeName(exchange)}</MobileExchange>
            <MobileInfo>
              <Head>
                <div>{intl.formatMessage({ id: 'MAIN.TARIFF' })}:</div>
                <div>{name}</div>
              </Head>
              <Head>
                <div>{intl.formatMessage({ id: 'MAIN.DAY_START' })}:</div>
                <div>{dayStart || '-'}</div>
              </Head>
              <Head>
                <div>{intl.formatMessage({ id: 'MAIN.DEPOSIT' })}:</div>
                <div>{`${deposit || '-'} $`}</div>
              </Head>
              <Head>
                <div> {intl.formatMessage({ id: 'MAIN.GET_HALF_MONTH' })}:</div>
                <div>{`${feeHalfMonthPercent || '-'} %`}</div>
              </Head>
              <Head>
                <div> {intl.formatMessage({ id: 'MAIN.TOTAL_GET' })}:</div>
                <div> {`${feeTotalPercent || '-'} %`}</div>
              </Head>
            </MobileInfo>
            <MobileButton>
              <MainButton
                width={80}
                title={
                  status === 'active'
                    ? intl.formatMessage({ id: 'TARIFF.DETAIL' })
                    : status === 'wait'
                    ? intl.formatMessage({ id: 'TARIFF.WAIT' })
                    : intl.formatMessage({ id: 'TARIFF.ACTIVATE' })
                }
                onClick={this.toggleCollapse}
                color={status === 'active' ? 'green' : 'orange'}
              />
            </MobileButton>
          </MobileTariffHeader>
        </Mobile>
        {/*<Default>Not mobile (desktop or laptop or tablet)</Default>*/}
      </AdaptiveTariffHeader>
    )
  }
}

export default injectIntl(TariffHeader)
