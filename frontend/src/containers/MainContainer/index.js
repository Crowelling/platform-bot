import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { createStructuredSelector } from 'reselect'
import Main from './Main'

import { fetchAllData, logout, setExchangeKeys } from '../../redux/actions'

import {
  getAllData,
  getUserInfo,
  getTotalInfo,
  getTariffInfo,
} from '../../redux/selectors'

const actions = {
  logout,
  fetchAllData,
  setExchangeKeys,
}
const selectors = createStructuredSelector({
  data: getAllData,
  userInfo: getUserInfo,
  totalInfo: getTotalInfo,
  tariffInfo: getTariffInfo,
})

export default withRouter(
  connect(
    selectors,
    actions,
  )(Main),
)
