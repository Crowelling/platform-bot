import React, { Component } from 'react'
import { injectIntl } from 'react-intl'

import Tariff from './tariffs/Tariff'

import { TOKEN } from '../../constants'
import { getItemAsync } from '../../redux/storage'
import {
  Row,
  Head,
  Title,
  Header,
  Footer,
  CardInfo,
  BannerText,
  Desktop,
  Tablet,
  FloatingAddButton,
} from '../../components'
import {
  Banner,
  BannerItem,
  Container,
  PageWrapper,
  Tariffs,
  TabHead,
  DesktopTableHead,
  TabHeadRow,
} from './styles'

class MainContainer extends Component {
  state = {
    tab: 'activeTab',
  }
  async componentDidMount() {
    const isLogged = await getItemAsync(TOKEN)
    if (!isLogged) {
      this.props.history.push('/')
      this.props.logout()
    } else {
      this.props.fetchAllData()
    }
  }

  renderUserInfo = () => {
    const { userInfo, totalInfo, intl } = this.props
    const { name, surname, status, phone, email } = userInfo
    const { totalSells, totalBuys, feeTotalUSD, feeHalfMonthUSD } = totalInfo

    return (
      <Banner>
        <BannerItem>
          <div style={{ marginLeft: '1em' }}>
            <BannerText>
              {name} {surname}
            </BannerText>
            <Title>{status}</Title>
            <Row top={3}>
              <Head>phone:</Head>
              <Head left={1.5}>{phone}</Head>
            </Row>
            <Row top={1}>
              <Head>mail:</Head>
              <Head left={1.5}>{email}</Head>
            </Row>
          </div>
        </BannerItem>
        <BannerItem top={4}>
          <CardInfo
            amount={totalSells}
            title={intl.formatMessage({ id: 'MAIN.TOTAL_SELLS' })}
          />
          <CardInfo
            amount={totalBuys}
            title={intl.formatMessage({ id: 'MAIN.TOTAL_BUYS' })}
          />
          <CardInfo
            amount={`${feeHalfMonthUSD} $`}
            title={intl.formatMessage({ id: 'MAIN.GET_HALF_MONTH' })}
          />
          <CardInfo
            amount={`${feeTotalUSD} $`}
            title={intl.formatMessage({ id: 'MAIN.TOTAL_GET' })}
          />
        </BannerItem>
      </Banner>
    )
  }

  renderTariffs = () => {
    const { tariffInfo, setExchangeKeys } = this.props
    return tariffInfo.map((tariff, index) => {
      return (
        <Tariff key={index} tariff={tariff} setExchangeKeys={setExchangeKeys} />
      )
    })
  }

  renderTabsHead = () => {
    const { tab } = this.state
    const { intl } = this.props
    return (
      <TabHeadRow>
        <TabHead
          isActive={tab === 'activeTab'}
          onClick={() => this.setState({ tab: 'activeTab' })}
        >
          {intl.formatMessage({ id: 'MAIN.ACTIVE_ROBOTS' })}
        </TabHead>
        <TabHead
          isActive={tab === 'historyTab'}
          onClick={() => this.setState({ tab: 'historyTab' })}
        >
          {intl.formatMessage({ id: 'MAIN.HISTORY_ROBOTS' })}
        </TabHead>
      </TabHeadRow>
    )
  }

  renderTableHead = screen => {
    const { intl } = this.props
    return (
      <DesktopTableHead>
        <Head>{intl.formatMessage({ id: 'MAIN.EXCHANGE' })}</Head>
        <Head>{intl.formatMessage({ id: 'MAIN.TARIFF' })}</Head>
        <Head>{intl.formatMessage({ id: 'MAIN.DAY_START' })}</Head>
        <Head>{intl.formatMessage({ id: 'MAIN.DEPOSIT' })}</Head>
        <Head>{intl.formatMessage({ id: 'MAIN.GET_HALF_MONTH' })}</Head>
        <Head>{intl.formatMessage({ id: 'MAIN.TOTAL_GET' })}</Head>
        {screen === 'desktop' && <Head></Head>}
      </DesktopTableHead>
    )
  }

  renderTabsBody = () => {
    const { intl } = this.props
    const { tab } = this.state

    if (tab === 'activeTab')
      return (
        <Tariffs>
          <DesktopTableHead>
            <Desktop>{this.renderTableHead('desktop')}</Desktop>
            <Tablet>{this.renderTableHead()}</Tablet>
          </DesktopTableHead>
          {this.renderTariffs()}
        </Tariffs>
      )

    return <Head>{intl.formatMessage({ id: 'MAIN.HISTORY_ROBOTS' })}</Head>
  }

  render() {
    return (
      <Container>
        <Header history={this.props.history} />
        <PageWrapper>
          {this.renderUserInfo()}

          {this.renderTabsHead()}
          {this.renderTabsBody()}
          <Footer />
        </PageWrapper>
        <FloatingAddButton />
      </Container>
    )
  }
}

export default injectIntl(MainContainer)
