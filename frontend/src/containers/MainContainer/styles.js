import styled from 'styled-components'
import banner from '../../assets/images/banner.jpg'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100vh;
  padding-bottom: 5em;
  background: ${({ theme }) =>
    `linear-gradient(${theme.colours.black}, ${theme.colours.blackLight})`}};
`

export const PageWrapper = styled.div`
  display: inline-block;
  width: -moz-available;
  width: -webkit-fill-available;
  width: fill-available;
  flex-direction: column;
  min-height: calc(100vh - 6em);
  overflow-x: scroll;
`

export const Tariffs = styled.div`
  display: block;
  padding: 2em;
  padding-bottom: 15em;
  flex-direction: column;
`

export const Banner = styled.div`
  display: inline-block;
  background-image: url(${banner});
  width: 100%;
  padding: 2em;
  justify-content: space-between;
  align-items: center;
  background-position: ${({ position }) => (position ? position : 'bottom')};
  background-size: cover;
  background-repeat: no-repeat;
`

export const BannerItem = styled.div`
  display: flex;
  padding: 0 2em;
  flex-direction: row;
  flex: 0 0 5em;
  flex-wrap: wrap;
  margin-top: ${({ top }) => (top ? `${top}em` : 'none')};
  margin-left: ${({ left }) => (left ? `${left}em` : 'none')};
  margin-right: ${({ right }) => (right ? `${right}em` : 'none')};
  margin-bottom: ${({ bottom }) => (bottom ? `${bottom}em` : 'none')};
`

export const TabHeadRow = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  padding: 1em;
  margin-left: 2em;
`

export const TabHead = styled.div`
  display: inline-block;
  cursor: pointer
  font-size: 1.3em;
  color: ${({ isActive, theme }) =>
    isActive ? theme.colours.orange : theme.colours.primaryText};
  flex-direction: row;
  padding: 0.5em 1em 0.5em 1em;
  margin-right: 1em;
  border-bottom: ${({ isActive, theme }) =>
    isActive ? `2px solid ${theme.colours.orange}` : 'none'};
`

export const DesktopTableHead = styled.div`
  display: inline-block;
  flex-direction: row;
  width: 100%;
  & > div {
    flex: 1;
    display: inline-flex;
    color: ${({ theme }) => theme.colours.orange};
    margin-left: 1em;
    text-align: center;
  }
`
