import React, { Component } from 'react'
import { injectIntl } from 'react-intl'

import { TOKEN } from '../../constants'
import { getItemAsync } from '../../redux/storage'
import landingMockup from '../../assets/images/landingMockup.png'
import {
  Iq,
  Container,
  PageWrapper,
  Banner,
  CenterText,
  LandingImage,
} from './styles'
import {
  ActionInput,
  Footer,
  Header,
  MainButton,
  Title,
} from '../../components'

class LandingContainer extends Component {
  async componentDidMount() {
    const isLogged = await getItemAsync(TOKEN)
    if (isLogged) {
      this.props.history.push('/')
    }
  }

  // eslint-disable-next-line
  shouldComponentUpdate(nextProps, nextState, nextContext) {
    if (nextProps.isAuth) {
      this.props.history.push('/')
      return false
    }
    return true
  }

  state = {
    token: '',
  }

  _handleKeyDown = e => {
    if (e.key === 'Enter') {
      this.props.signIn({ token: this.state.token })
    }
  }

  changeToken = e => {
    this.setState({ token: e.target.value })
  }

  verifyToken = () => {
    this.props.signIn({ token: this.state.token })
  }

  getKey = () => {
    // todo redirect to market page
  }

  render() {
    const { token } = this.state
    const { intl } = this.props
    return (
      <Container>
        <Header />
        <PageWrapper>
          <Banner>
            <CenterText>
              <Title>
                <Iq>IQuandex</Iq>{' '}
                {intl.formatMessage({ id: 'LANDING.BANNER_EXPLAIN' })}
              </Title>
              <Title top={2}>
                {intl.formatMessage({ id: 'LANDING.BANNER_GET_ROBOT' })}{' '}
                <Iq>IQuandex</Iq>{' '}
                {intl.formatMessage({ id: 'LANDING.BANNER_TWO_CLICK' })}
              </Title>
            </CenterText>
            <ActionInput
              top={4}
              bottom={2}
              width={40}
              value={token}
              actionTitle={intl.formatMessage({ id: 'LANDING.GET_STARTED' })}
              actionColor="green"
              placeholder={intl.formatMessage({ id: 'LANDING.YOUR_KEY' })}
              onClick={this.verifyToken}
              onChange={this.changeToken}
              onKeyDown={this._handleKeyDown}
            />
          </Banner>

          <CenterText>
            <Title bottom={3}>
              {intl.formatMessage({ id: 'LANDING.CENTER_TRADE' })}
              <Iq>IQuandex</Iq>
              {intl.formatMessage({ id: 'LANDING.CENTER_50' })}
            </Title>
          </CenterText>
          <LandingImage src={landingMockup} />
          <Banner>
            <CenterText>
              <Title top={2}>
                {intl.formatMessage({ id: 'LANDING.BOTTOM_SECURITY' })}
              </Title>
              <Title top={1}>
                {intl.formatMessage({ id: 'LANDING.BOTTOM_FROM_MOMENT' })}
                {intl.formatMessage({ id: 'LANDING.BOTTOM_TILL_FEE' })}
              </Title>
              <div>
                <MainButton
                  top={3}
                  width={50}
                  fill="true"
                  color="green"
                  textColor="primaryText"
                  title={intl.formatMessage({ id: 'LANDING.GET_KEY' })}
                  onClick={this.getKey}
                />
              </div>
            </CenterText>
            <Footer style={{ marginTop: '3em' }} />
          </Banner>
        </PageWrapper>
      </Container>
    )
  }
}

export default injectIntl(LandingContainer)
