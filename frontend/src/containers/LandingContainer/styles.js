import styled from 'styled-components'
import banner from '../../assets/images/banner.jpg'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100vh;
  background: ${({ theme }) =>
    `linear-gradient(${theme.colours.black}, ${theme.colours.blackLight})`}};
`

export const PageWrapper = styled.div`
  display: flex;
  align-items: center;
  width: -moz-available;
  width: -webkit-fill-available;
  width: fill-available;
  flex-direction: column;
  min-height: calc(100vh - 6em);
  overflow-x: scroll;
`

export const Banner = styled.div`
  display: block;
  flex-direction: column;
  background-image: url(${banner});
  width: 100%;
  padding: 2em;
  align-items: center;
  justify-content: center;
  text-align: center;
  background-position: ${({ position }) => (position ? position : 'bottom')};
  background-size: cover;
  background-repeat: no-repeat;
`

export const CenterText = styled.div`
  width: 70%;
  display: inline-block;
  align-items: center;
  justify-content: center;
  text-align: center;
`

export const Iq = styled.span`
  font-size: 40px;
  font-weight: 500;
  color: ${({ theme }) => theme.colours.orange};
`

export const LandingImage = styled.img`
  width: 70%;
  margin: 1em;
  height: auto;
  object-fit: cover;
`
