import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { createStructuredSelector } from 'reselect'

import Landing from './Landing'

import { signIn } from '../../redux/actions'
import { getUserAuthStatus } from '../../redux/selectors'

const actions = {
  signIn,
}
const selectors = createStructuredSelector({
  isAuth: getUserAuthStatus,
})

export default withRouter(
  connect(
    selectors,
    actions,
  )(Landing),
)
