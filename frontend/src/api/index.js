import User from './user'
import Main from './main'

import ApiClient from './apiClient'

const apiClient = new ApiClient()

export default {
  client: apiClient,

  main: new Main(apiClient),
  user: new User(apiClient),
}
