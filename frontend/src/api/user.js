import Base from './base'

export default class User extends Base {
  signIn(token) {
    return this.apiClient.get('signin', token).then(response => {
      return response.data
    })
  }

  setExchangeKeys(data) {
    return this.apiClient.post('setExchangeKeys', data).then(response => {
      return response
    })
  }
}
