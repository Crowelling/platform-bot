import axios from 'axios'
import { API_URL, TOKEN } from '../../constants'
import {
  getItemAsync,
  setItemAsync,
  removeItemAsync,
} from '../../redux/storage'

export default class ApiClient {
  constructor() {
    ApiClient.singleton = this

    getItemAsync(TOKEN).then(res => {
      this.token = res || null
    })

    this.client = axios.create({
      baseURL: API_URL,
      timeout: 10000,
      headers: {
        'Access-Control-Allow-Origin': '*',
        token: this.token,
      },
    })

    this.client.interceptors.request.use(
      function(config) {
        // eslint-disable-next-line no-undef
        let token = localStorage.getItem(TOKEN) || ''
        if (token) {
          config.headers.token = token
        }

        return config
      },
      function(error) {
        return Promise.reject(error)
      },
    )
  }

  setToken(token) {
    this.token = token
    setItemAsync(TOKEN, token)
  }

  removeToken() {
    this.token = null
    removeItemAsync(TOKEN)
  }

  get(url, headers) {
    return this._request({ url, method: 'get', headers })
  }

  post(url, body, headers) {
    return this._request({ url, method: 'post', body, headers })
  }

  patch(url, body, headers) {
    return this._request({ url, method: 'patch', body, headers })
  }

  put(url, body, headers) {
    return this._request({ url, method: 'put', body, headers })
  }

  delete(url, body, headers) {
    return this._request({ url, method: 'delete', body, headers })
  }

  _request({ url, method, params, body, headers }) {
    method = method.toLowerCase()
    let data = {}
    if (body) data = body
    if (params) data = params
    // this works if get method
    if (headers) data.headers = { ...data.headers, ...headers }
    return this.client[method](`/${url}`, data, { headers: headers }).then(
      ({ data }) => data,
    )
  }
}
