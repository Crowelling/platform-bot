import Base from './base'

export default class Main extends Base {
  fetchAllData() {
    return this.apiClient.get('main').then(response => {
      return response
    })
  }
}
