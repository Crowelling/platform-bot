import React, { PureComponent } from 'react'
import propTypes from 'prop-types'
import {
  Icon,
  StyledCheckbox,
  HiddenCheckbox,
  CheckBoxWrapper,
  CheckboxContainer,
  IndeterminateIcon,
} from './styles'

class CheckBox extends PureComponent {
  render() {
    const {
      label,
      checked,
      disabled,
      color,
      margin,
      selector,
      indeterminate,
      ...props
    } = this.props
    return (
      <CheckBoxWrapper
        data-selector={selector}
        disabled={disabled}
        margin={margin}
      >
        <CheckboxContainer>
          <HiddenCheckbox
            data-selector={selector}
            disabled={disabled}
            checked={checked}
            {...props}
          />
          <StyledCheckbox
            data-selector={selector}
            disabled={disabled}
            checked={checked}
            color={color}
          >
            {indeterminate ? (
              <IndeterminateIcon />
            ) : (
              <Icon viewBox="0 0 24 24">
                <polyline points="20 6 9 17 4 12" />
              </Icon>
            )}
          </StyledCheckbox>
        </CheckboxContainer>

        {label}
      </CheckBoxWrapper>
    )
  }
}

CheckBox.propTypes = {
  checked: propTypes.bool.isRequired,
  onChange: propTypes.func.isRequired,
}

CheckBox.defaultProps = {
  indeterminate: false,
}

export default CheckBox
