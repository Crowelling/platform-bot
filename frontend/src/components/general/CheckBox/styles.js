import styled, { css } from 'styled-components'

export const CheckBoxWrapper = styled.label`
  display: flex;
  position: relative;
  padding-left: 1em;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  font-family: Roboto;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  font-size: 0.875rem;
  color: ${props => (props.disabled ? '#a0a4a8' : '#323237')};
`

export const CheckboxContainer = styled.div`
  display: inline-block;
  vertical-align: middle;
  margin-right: 0.5em;
  height: 0em;
`

export const HiddenCheckbox = styled.input.attrs({ type: 'checkbox' })`
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 0;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 0;
`

export const Icon = styled.svg`
  fill: none;
  stroke: white;
  stroke-width: 2px;
`

const checkedEnabledStyle = css`
  background: ${props =>
    props.color
      ? props.color
      : 'linear-gradient(to left, rgba(255, 163, 99, 1.0), rgba(255, 199, 93, 1.0))'};
  border: solid 1px
    linear-gradient(to left, rgba(255, 163, 99, 1), rgba(255, 199, 93, 1));
  cursor: pointer;

  ${Icon} {
    visibility: visible;
  }
`
const uncheckEnabledStyle = css`
  background: #fff;
  border: solid 1px #ffcc5c;
  cursor: pointer;

  ${HiddenCheckbox}:hover + & {
    background-color: #ccc;
  }

  ${Icon} {
    visibility: hidden;
  }
`

const checkedDisabledStyle = css`
  background: #dadada;
  border: solid 1px transparent;

  ${Icon} {
    visibility: visible;
  }
`
const uncheckDisabledStyle = css`
  background: #fff;
  border: solid 1px #dadada;

  ${Icon} {
    visibility: hidden;
  }
`

const disabledStyle = css`
  ${props => (props.checked ? checkedDisabledStyle : uncheckDisabledStyle)};
`

const enabledStyle = css`
  ${props => (props.checked ? checkedEnabledStyle : uncheckEnabledStyle)};
`

export const StyledCheckbox = styled.div`
  display: inline-block;
  width: 16px;
  height: 16px;
  border-radius: 3px;
  transition: all 100ms;

  ${props => (props.disabled ? disabledStyle : enabledStyle)};
`

export const IndeterminateIcon = styled.div`
  width: 15px;
  height: 15px;
  background-color: #a0a4a8;
  border-radius: 3px;
  position: relative;
  &::after {
    content: '';
    position: absolute;
    top: 50%;
    left: 50%;
    background-color: #fff;
    width: 9px;
    height: 2px;
    transform: translate(-50%, -50%);
  }
`
