import styled from 'styled-components'

const BaseTextStyles = styled.div`
  flex: ${({ flex }) => (flex ? flex : 'none')};
  font-family: font-family: 'Montserrat', sans-serif;
  font-style: normal;
  font-stretch: normal;
  line-height: normal;
  letter-spacing: normal;
  word-break: break-word;
  white-space: pre-line;
  text-align: ${({ align }) => (align ? align : 'inherit')};
  width: ${({ width }) => (width ? `${width}%` : 'auto')};
  margin-top: ${({ top }) => (top ? `${top}em` : 'none')};
  margin-bottom: ${({ bottom }) => (bottom ? `${bottom}em` : 'none')};
  margin-left: ${({ left }) => (left ? `${left}em` : 'none')};
  margin-right: ${({ right }) => (right ? `${right}em` : 'none')};
  color: ${({ color, theme }) =>
    color ? theme.colours[color] : theme.colours.primaryText};
`

export const BannerText = styled(BaseTextStyles)`
  font-size: ${({ theme }) => theme.fontSizes['42px']};
  font-weight: 500;
`

export const Title = styled(BaseTextStyles)`
  font-size: ${({ theme }) => theme.fontSizes['20px']};
  font-weight: 500;
`

export const Head = styled(BaseTextStyles)`
  font-size: ${({ theme }) => theme.fontSizes['16px']};
  font-weight: 500;
`

export const Subhead = styled(BaseTextStyles)`
  font-size: ${({ theme }) => theme.fontSizes['14px']};
  font-weight: normal;
`

export const Text = styled(BaseTextStyles)`
  font-size: ${({ theme }) => theme.fontSizes['14px']};
  font-weight: 500;
`

export const Description = styled(BaseTextStyles)`
  font-size: ${({ theme }) => theme.fontSizes['12px']};
  font-weight: normal;
  color: ${({ color, theme }) =>
    color ? theme.colours[color] : theme.colours.greyDisabled};
`
export const Row = styled(BaseTextStyles)`
  display: flex;
  flex-direction: row;
`
