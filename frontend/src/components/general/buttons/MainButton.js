import React, { PureComponent } from 'react'
import propTypes from 'prop-types'
import styled from 'styled-components'

const ButtonWrapper = styled.button`
  display: inline-block;
  align-items: center;
  justify-content: center;
  width: ${({ width }) => (width ? `${width}%` : 'initial')};
  padding: 0 29px;
  height: 3em;
  min-height: 3em;
  align-self: center;
  background: ${({ fill, color, theme }) =>
    fill ? theme.colours[color] : `transparent`};
  cursor: ${({ disabled }) => (disabled ? 'initial' : 'pointer')};
  outline-width: 0;
  border: 1px solid
    ${({ theme, color }) =>
      color ? theme.colours[color] : theme.colours.orange};
  border-radius: 2em;
  cursor: ${({ disabled }) => (disabled ? 'initial' : 'pointer')};
  margin-top: ${props => (props.top ? `${props.top}em` : 'none')};
  margin-left: ${props => (props.left ? `${props.left}em` : 'none')};
  margin-right: ${props => (props.right ? `${props.right}em` : 'none')};
  margin-bottom: ${props => (props.bottom ? `${props.bottom}em` : 'none')};
  outline-width: 0;
  font-size: ${props => props.theme.fontSizes['14px']};
  font-weight: 500;
  text-align: center;
  color: ${({ theme, color, fill }) =>
    fill
      ? theme.colours.primaryText
      : color
      ? theme.colours[color]
      : theme.colours.orange};

  &:hover {
    background: ${({ theme, color }) =>
      color ? theme.colours[color] : theme.colours.orange};
    color: ${({ theme }) => theme.colours.primaryText};
  }
`

class MainButton extends PureComponent {
  render() {
    const {
      id,
      top,
      left,
      right,
      style,
      height,
      width,
      title,
      fill,
      textColor,
      color,
      bottom,
      onClick,
      disabled,
    } = this.props
    return (
      <ButtonWrapper
        id={id}
        top={top}
        left={left}
        right={right}
        bottom={bottom}
        style={style}
        width={width}
        fill={fill}
        color={color}
        textColor={textColor}
        height={height}
        onClick={onClick}
        disabled={disabled}
      >
        {title}
      </ButtonWrapper>
    )
  }
}

MainButton.propTypes = {
  title: propTypes.string,
  onClick: propTypes.func,
}

MainButton.defaultProps = {
  title: 'OK',
  onClick: () => {},
}

export default MainButton
