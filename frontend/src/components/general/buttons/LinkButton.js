import React, { PureComponent } from 'react'
import propTypes from 'prop-types'
import styled from 'styled-components'

const LinkButtonWrapper = styled.button`
  display: flex;
  align-items: center;
  justify-content: ${props => (props.align ? props.align : 'flex-start')};
  width: fit-content;
  background: transparent;
  border: ${({ border, theme }) =>
    border ? `1px solid ${theme.colours.secondary}` : 'none'};
  padding: ${props => (props.padding ? props.padding : 'inherit')};
  border-radius: 2em;
  height: fit-content;
  color: ${({ color, disabled, theme }) =>
    color
      ? color
      : disabled
      ? theme.colours.greyDisabled
      : theme.colours.secondary};
  cursor: ${props => (props.disabled ? 'initial' : 'pointer')};
  outline-width: 0;
  font-size: ${props => props.theme.fontSizes['14px']};
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  letter-spacing: normal;
  text-align: center;
  align-items: center;
  margin-top: ${props => (props.top ? `${props.top}em` : 'none')};
  margin-left: ${props => (props.left ? `${props.left}em` : 'none')};
  margin-right: ${props => (props.right ? `${props.right}em` : 'none')};
  margin-bottom: ${props => (props.bottom ? `${props.bottom}em` : 'none')};

  &:hover {
    opacity: ${props => (props.disabled ? '1' : '0.7')};
  }
`

export const PlusWrapper = styled.div`
  display: ${props => (props.plus ? 'flex' : 'none')};
  font-size: ${props => props.theme.fontSizes['14px']};
  margin-right: 0.3em;
`

class LinkButton extends PureComponent {
  render() {
    const {
      id,
      top,
      plus,
      left,
      right,
      style,
      title,
      border,
      bottom,
      padding,
      onClick,
      disabled,
    } = this.props
    return (
      <LinkButtonWrapper
        id={id}
        top={top}
        left={left}
        right={right}
        bottom={bottom}
        padding={padding}
        border={border}
        style={style}
        onClick={onClick}
        disabled={disabled}
      >
        <PlusWrapper plus={plus}>+</PlusWrapper>

        {title}
      </LinkButtonWrapper>
    )
  }
}

LinkButton.propTypes = {
  title: propTypes.string.isRequired,
  onClick: propTypes.func.isRequired,
}

export default LinkButton
