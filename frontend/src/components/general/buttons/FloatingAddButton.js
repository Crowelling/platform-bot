import React, { PureComponent } from 'react'
import propTypes from 'prop-types'
import styled from 'styled-components'

const Container = styled.div`
  position: fixed;
  bottom: 2em;
  right: 2em;
`

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  background: ${({ theme }) => theme.colours.orange};
  padding: ${props => (props.padding ? props.padding : 'inherit')};
  border-radius: 100%;
  height: 1.2em;
  width: 1.2em;
  color: ${({ color, disabled, theme }) =>
    color
      ? color
      : disabled
      ? theme.colours.greyDisabled
      : theme.colours.primaryText};
  cursor: ${props => (props.disabled ? 'initial' : 'pointer')};
  font-size: ${props => props.theme.fontSizes['14px']};
  font-weight: 500;
  font-style: normal;
  font-stretch: normal;
  letter-spacing: normal;
  text-align: center;
  margin-top: ${({ top }) => (top ? `${top}em` : 'none')};
  margin-left: ${({ left }) => (left ? `${left}em` : 'none')};
  margin-right: ${({ right }) => (right ? `${right}em` : 'none')};
  margin-bottom: ${({ bottom }) => (bottom ? `${bottom}em` : 'none')};
  font-size: ${({ theme }) => theme.fontSizes['60px']};
  &:hover {
    opacity: ${props => (props.disabled ? '1' : '0.7')};
  }
`

class FloatingAddButton extends PureComponent {
  render() {
    const {
      id,
      top,
      left,
      right,
      style,
      bottom,
      padding,
      onClick,
      disabled,
    } = this.props
    return (
      <Container>
        <Wrapper
          id={id}
          top={top}
          left={left}
          right={right}
          bottom={bottom}
          padding={padding}
          style={style}
          onClick={onClick}
          disabled={disabled}
        >
          +
        </Wrapper>
      </Container>
    )
  }
}

FloatingAddButton.propTypes = {
  title: propTypes.string,
  onClick: propTypes.func,
}

export default FloatingAddButton
