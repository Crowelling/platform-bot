import React, { Component } from 'react'
import Popover from '@material-ui/core/Popover'
import styled from 'styled-components'

import { Title } from '../../index'

import globe from '../../../assets/images/globe.png'

const Globe = styled.img`
  height: 24x;
  width: 24px;
  cursor: pointer;
`

const Languages = styled.div`
  flex-direction: column;
  display: flex;
  justify-content: space-between;
  width: 10em;
  height: 7em;
  padding: 1em;
  border-radius: 2px;
  border: 1px solid #141315;
  background-color: #222022;
  box-shadow: 0 1px 15px rgba(0, 0, 0, 0.1), 0 1px 4px rgba(0, 0, 0, 1);
  & > div {
    cursor: pointer;
  }
`

class LanguagesButton extends Component {
  state = {
    isOpen: false,
    anchorEl: null,
  }

  toggleMenu = e => {
    if (e.currentTarget === this.state.anchorEl && this.state.isOpen) return
    this.setState({
      anchorEl: e.currentTarget,
      isOpen: !this.state.isOpen,
    })
  }

  render() {
    const { anchorEl, isOpen } = this.state
    const { changeLanguage } = this.props

    return (
      <div onClick={this.toggleMenu} ref={this.anchorEl}>
        <Globe src={globe} />
        <Popover
          open={isOpen}
          anchorEl={anchorEl}
          onClose={this.toggleMenu}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          <Languages>
            <Title onClick={() => changeLanguage('en')}>English</Title>
            <Title onClick={() => changeLanguage('ru')}>Русский</Title>
          </Languages>
        </Popover>
      </div>
    )
  }
}

export default LanguagesButton
