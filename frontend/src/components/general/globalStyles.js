/* eslint no-unused-expressions: 0 */
import { createGlobalStyle } from 'styled-components'

createGlobalStyle`
  html {
      font-size: 62.5%;
  }
  
  body {
    margin: 0;
    padding: 0;
    font-size: 14px;
    font-size: 1rem;
    box-sizing: border-box;
    font-family: 'Montserrat', sans-serif;
    color: #323237;

    *, *:before, *:after {
      box-sizing: inherit;
    }
  }

  button {
      outline: none;
  }
  
  ul {
    padding: 0;
    margin: 0;
    list-style: none;
  }

  .activity-form-modal {
    .rodal-close {
      top: 32px;
      right: 28px;
    }
  }

  .range-slider-activity-form {
    width: 100%;
    height: 32px;
  }
`
