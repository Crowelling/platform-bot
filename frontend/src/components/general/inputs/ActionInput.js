import React, { PureComponent } from 'react'
import styled from 'styled-components'
import propTypes from 'prop-types'

const Container = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  align-items: center;
  justify-content: center;
  margin-top: ${({ top }) => (top ? `${top}em` : 'none')};
  margin-bottom: ${({ bottom }) => (bottom ? `${bottom}em` : 'none')};
  margin-left: ${({ left }) => (left ? `${left}em` : 'none')};
  margin-right: ${({ right }) => (right ? `${right}em` : 'none')};
`

const InputContainer = styled.input`
  height: 3em;
  border-top-left-radius: 2px;
  border-bottom-left-radius: 2px;
  font-size: 1em;
  line-height: 20px;
  box-sizing: border-box;
  border: 1px solid ${({ theme }) => theme.colours.borderInput};
  color: ${({ theme }) => theme.colours.gray};
  outline: none;
  padding: 14px 16px;
  -webkit-transition: all 0.15s ease-in-out;
  transition: all 0.15s ease-in-out;
  width: ${({ width }) => (width ? `${width}%` : '100%')};
  cursor: text;
  ::-webkit-inner-spin-button,
  ::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  background: #232123;
  margin-top: ${props => (props.top ? `${props.top}em` : 'none')};
  margin-left: ${props => (props.left ? `${props.left}em` : 'none')};
  margin-right: ${props => (props.right ? `${props.right}em` : 'none')};
  margin-bottom: ${props => (props.bottom ? `${props.bottom}em` : 'none')};
`

const ActionButton = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => (props.width ? props.width : 'initial')};
  padding: 0 29px;
  height: 3em;
  background: transparent;
  cursor: ${({ disabled }) => (disabled ? 'initial' : 'pointer')};
  outline-width: 0;
  border: 1px solid
    ${({ theme, color }) =>
      color ? theme.colours[color] : theme.colours.orange};
  border-top-right-radius: 2em;
  border-bottom-right-radius: 2em;
  cursor: ${({ disabled }) => (disabled ? 'initial' : 'pointer')};
  outline-width: 0;
  font-size: ${props => props.theme.fontSizes['14px']};
  font-weight: 500;
  text-align: center;
  color: ${({ theme, color }) =>
    color ? theme.colours[color] : theme.colours.orange};

  &:hover {
    background: ${({ theme, color }) =>
      color ? theme.colours[color] : theme.colours.orange};
    color: ${({ theme }) => theme.colours.primaryText};
  }
`

class ActionInput extends PureComponent {
  render() {
    const {
      top,
      left,
      right,
      bottom,
      type,
      value,
      width,
      onChange,
      disabled,
      placeholder,
      onClick,
      actionTitle,
      actionColor,
      onKeyDown,
    } = this.props
    return (
      <Container top={top} left={left} right={right} bottom={bottom}>
        <InputContainer
          type={type}
          value={value}
          width={width}
          onChange={onChange}
          disabled={disabled}
          placeholder={placeholder}
          onKeyDown={onKeyDown}
        />
        <ActionButton
          color={actionColor}
          onClick={!value.length ? () => {} : onClick}
        >
          {actionTitle}
        </ActionButton>
      </Container>
    )
  }
}

ActionInput.propTypes = {
  value: propTypes.string.isRequired,
  onChange: propTypes.func.isRequired,
  actionTitle: propTypes.string,
  onClick: propTypes.func,
  actionColor: propTypes.string,
}

export default ActionInput
