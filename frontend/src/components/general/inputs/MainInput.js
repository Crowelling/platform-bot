import React, { PureComponent } from 'react'
import styled from 'styled-components'
import propTypes from 'prop-types'

const Wrapper = styled.div`
  position: relative;
  margin-top: ${({ top }) => (top ? `${top}em` : 'none')};
  margin-bottom: ${({ bottom }) => (bottom ? `${bottom}em` : 'none')};
  margin-left: ${({ left }) => (left ? `${left}em` : 'none')};
  margin-right: ${({ right }) => (right ? `${right}em` : 'none')};
`

const InputContainer = styled.input`
  height: 3em;
  font-size: 1em;
  line-height: 20px;
  -webkit-transition: all 0.15s ease-in-out;
  transition: all 0.15s ease-in-out;
  width: 100%;
  border: none;
  cursor: text;
  outline: none;
  ::-webkit-inner-spin-button,
  ::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  background: transparent;
  padding: 0 10px;
  color: ${({ theme }) => theme.colours.gray};
  border: 1px solid
    ${({ colorInput, theme }) =>
      colorInput ? theme.colours[colorInput] : theme.colours.borderInput};
  border-radius: 2px;

  &:focus {
    border: 1px solid ${({ theme }) => theme.colours.orange};
  }
`

const Legend = styled.div`
  color: ${({ colorInput, theme }) =>
    colorInput ? theme.colours[colorInput] : theme.colours.borderInput};
  padding: 0 15px;
  position: absolute;
  top: -7px;
  left: 10px;
  -webkit-transition: all 0.15s ease-in-out;
  transition: all 0.15s ease-in-out;
  background-color: ${({ theme }) => theme.colours.blackLight};
  ${InputContainer}:focus ~ & {
    color: ${({ theme }) => theme.colours.orange};
  }
`

class Input extends PureComponent {
  render() {
    const {
      top,
      left,
      right,
      bottom,
      type,
      value,
      legend,
      onChange,
      disabled,
      placeholder,
      colorInput,
    } = this.props
    return (
      <Wrapper top={top} left={left} right={right} bottom={bottom}>
        <InputContainer
          type={type}
          value={value}
          onChange={onChange}
          disabled={disabled}
          placeholder={placeholder}
          colorInput={colorInput}
        />
        <Legend colorInput={colorInput}>{legend}</Legend>
      </Wrapper>
    )
  }
}

Input.propTypes = {
  onChange: propTypes.func.isRequired,
  legend: propTypes.string.isRequired,
  value: propTypes.string.isRequired,
  placeholder: propTypes.string,
}

Input.defaultProps = {
  onChange: () => {},
  legend: 'Input',
  value: '',
  placeholder: '...',
}

export default Input
