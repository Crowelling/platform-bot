import React, { PureComponent } from 'react'
import propTypes from 'prop-types'
import { Head, Title } from '../Text'
import { Card } from './index'

class CardInfo extends PureComponent {
  render() {
    const { top, left, right, bottom, title, amount } = this.props
    return (
      <Card top={top} left={left} right={right} bottom={bottom} direction="row">
        <Head>{title}</Head>
        <Title color="orange">{amount}</Title>
      </Card>
    )
  }
}

CardInfo.propTypes = {
  title: propTypes.any.isRequired,
  amount: propTypes.any.isRequired,
}

export default CardInfo
