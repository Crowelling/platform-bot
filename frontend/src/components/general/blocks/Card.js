import React, { PureComponent } from 'react'
import styled from 'styled-components'

const Container = styled.div`
  flex: 1;
  display: flex;
  margin: 1em;
  align-items: center;
  justify-content: space-between;
  flex-direction: ${({ direction }) => (direction ? direction : 'row')};
  min-height: 5em;
  padding: 1em;
  border-radius: 2px;
  font-size: ${({ theme }) => theme.fontSizes['14px']};
  background: rgba(35, 33, 35, 0.5);
  font-weight: 500;
  min-width: 20em;
  flex: 1 1 20em;
  margin-top: ${({ top }) => (top ? `${top}em` : 'none')};
  margin-left: ${({ left }) => (left ? `${left}em` : 'none')};
  margin-right: ${({ right }) => (right ? `${right}em` : 'none')};
  margin-bottom: ${({ bottom }) => (bottom ? `${bottom}em` : 'none')};
  box-shadow: 0 1px 15px rgba(0, 0, 0, 0.1), 0 1px 4px rgba(0, 0, 0, 1);
`
class Card extends PureComponent {
  render() {
    const { top, left, right, bottom, direction, children } = this.props
    return (
      <Container
        top={top}
        left={left}
        right={right}
        bottom={bottom}
        direction={direction}
      >
        {children}
      </Container>
    )
  }
}

export default Card
