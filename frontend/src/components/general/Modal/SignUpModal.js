import React, { Component } from 'react'
import propTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { injectIntl } from 'react-intl'

import { Modal } from './index'
import { MainButton, MainInput, LinkButton, CheckBox, Subhead } from '../index'

const ContentWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 100%;
  padding: 10px 0px 20px 0px;
`

const ButtonsWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  width: 100%;
  margin-top: 2em;
  justify-content: center;
`

class SignUpModal extends Component {
  state = {
    email: '',
    password: '',
    confirmPassword: '',
    referenceId: '',
    acceptRules: false,
  }

  handleInputChange = (e, key) => {
    this.setState({ [key]: e.target.value })
  }

  handleSignUpValidation = () => {}

  render() {
    const { intl, isVisible, onClose, onSignIn } = this.props
    const {
      email,
      password,
      confirmPassword,
      referenceId,
      acceptRules,
    } = this.state
    return (
      <Modal
        isOpen={isVisible}
        onClose={onClose}
        title={intl.formatMessage({ id: 'SIGN_UP.REGISTRATION' })}
      >
        <ContentWrapper>
          <MainInput
            value={email}
            placeholder={intl.formatMessage({ id: 'SIGN_UP.EMAIL' })}
            bottom={1}
            onChange={e => this.handleInputChange(e, 'email')}
          />
          <MainInput
            value={password}
            placeholder={intl.formatMessage({ id: 'SIGN_UP.PASSWORD' })}
            bottom={1}
            onChange={e => this.handleInputChange(e, 'password')}
          />
          <MainInput
            value={confirmPassword}
            placeholder={intl.formatMessage({ id: 'SIGN_UP.CONFIRM_PASSWORD' })}
            bottom={1}
            onChange={e => this.handleInputChange(e, 'confirmPassword')}
          />
          <MainInput
            value={referenceId}
            placeholder={intl.formatMessage({ id: 'SIGN_UP.REFERENCE_ID' })}
            bottom={2}
            onChange={e => this.handleInputChange(e, 'referenceId')}
          />
          <div style={{ display: 'flex' }}>
            <CheckBox
              checked={acceptRules}
              onChange={() => {
                this.setState({ acceptRules: !this.state.acceptRules })
              }}
            />
            <LinkButton
              title={intl.formatMessage({
                id: 'SIGN_UP.TERMS_AND_CONDITIONALS',
              })}
              onClick={() => {
                alert('Terms and conditionals')
              }}
            />
          </div>
        </ContentWrapper>
        <ButtonsWrapper>
          <Subhead>
            {intl.formatMessage({ id: 'SIGN_UP.ALREADY_SIGNED' })}
          </Subhead>
          <LinkButton
            right={4}
            left={0.5}
            title={intl.formatMessage({ id: 'SIGN_UP.LOG_IN' })}
            onClick={onSignIn}
          />
          <MainButton
            title={intl.formatMessage({ id: 'SIGN_UP.NEXT' })}
            onClick={() => {}}
          />
        </ButtonsWrapper>
      </Modal>
    )
  }
}

SignUpModal.propTypes = {
  isVisible: propTypes.bool,
  onSignUp: propTypes.func,
  onSignIn: propTypes.func,
  onClose: propTypes.func,
}

SignUpModal.defaultProps = {
  isVisible: false,
  onSignUp: () => {},
  onSignIn: () => {},
  onClose: () => {},
}

const mapDispatchToProps = {}

export default connect(
  null,
  mapDispatchToProps,
)(injectIntl(SignUpModal))
