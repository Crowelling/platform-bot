import React, { Component } from 'react'
import propTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { injectIntl } from 'react-intl'

import { Modal } from './index'
import { MainButton, MainInput, LinkButton } from '../index'

const ContentWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 100%;
  padding: 10px 0px 20px 0px;
`

const ButtonsWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  width: 100%;
  justify-content: center;
`

class SignInModal extends Component {
  state = {
    email: '',
    password: '',
  }

  handleInputChange = (e, key) => {
    this.setState({ [key]: e.target.value })
  }

  handleSignInValidation = () => {}

  render() {
    const { intl, isVisible, onClose, onSignIn, onSignUp } = this.props
    const { email, password } = this.state
    return (
      <Modal
        isOpen={isVisible}
        onClose={onClose}
        title={intl.formatMessage({ id: 'SIGN_IN.ENTER_PRIVATE_CABINET' })}
      >
        <ContentWrapper>
          <MainInput
            value={email}
            placeholder={intl.formatMessage({ id: 'SIGN_IN.EMAIL' })}
            bottom={1}
            onChange={e => this.handleInputChange(e, 'email')}
          />
          <MainInput
            value={password}
            placeholder={intl.formatMessage({ id: 'SIGN_IN.PASSWORD' })}
            onChange={e => this.handleInputChange(e, 'password')}
          />
        </ContentWrapper>
        <ButtonsWrapper>
          <MainButton
            title={intl.formatMessage({ id: 'SIGN_IN.LOG_IN' })}
            onClick={onSignIn}
          />
          <LinkButton
            top={1}
            title={intl.formatMessage({ id: 'SIGN_IN.REGISTRATION' })}
            onClick={onSignUp}
          />
        </ButtonsWrapper>
      </Modal>
    )
  }
}

SignInModal.propTypes = {
  isVisible: propTypes.bool,
  onSignUp: propTypes.func,
  onSignIn: propTypes.func,
  onClose: propTypes.func,
}

SignInModal.defaultProps = {
  isVisible: false,
  onSignUp: () => {},
  onSignIn: () => {},
  onClose: () => {},
}

const mapDispatchToProps = {}

export default connect(
  null,
  mapDispatchToProps,
)(injectIntl(SignInModal))
