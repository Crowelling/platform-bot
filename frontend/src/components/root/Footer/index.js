import React, { Component } from 'react'
import { injectIntl } from 'react-intl'
import styled from 'styled-components'

const FooterWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 3em;
  padding: 1em;
  font-size: 20px;
  color: white;
  align-items: center;
  justify-content: center;
`

const Narfex = styled.a`
  font-size: 20px;
  color: orange;
  margin-left: 10px;
  cursor: pointer;
  text-decoration: none;
`

class Footer extends Component {
  render() {
    const { intl, style } = this.props
    return (
      <FooterWrapper style={style}>
        {intl.formatMessage({ id: 'FOOTER.NARFEX_SUPPORT' })}
        <Narfex href="https://narfex.com/">Narfex</Narfex>
      </FooterWrapper>
    )
  }
}

export default injectIntl(Footer)
