import styled from 'styled-components'

export const HeaderWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  background: ${({ theme }) => theme.colours.blackLight};
  width: 100%;
  height: 6em;
  min-height: 6em;
  padding: 1em;
  align-items: center;
  justify-content: space-between;
  & > div {
    flex: 1;
    display: flex;
    justify-content: flex-end;
  }
`

export const HeaderRight = styled.div`
  margin-top: 15px;
`

export const Logo = styled.img`
  height: 13em;
  width: auto;
`

export const Logout = styled.img`
  height: 24px;
  width: 24px;
  cursor: pointer;
  margin-right: 10px;
  margin-left: 30px;
`
