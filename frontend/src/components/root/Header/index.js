import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import { createStructuredSelector } from 'reselect'

import { LanguagesButton } from '../../general'
import { getUserAuthStatus } from '../../../redux/selectors'
import { changeLanguage, logout } from '../../../redux/actions'
import { Logo, Logout, HeaderRight, HeaderWrapper } from './styles'

import logoutImg from '../../../assets/images/logout.png'

import logo from '../../../assets/icons/logoBig.svg'

class Header extends Component {
  handleLogout = async () => {
    await this.props.logout()
    this.props.history.push('/landing')
  }

  render() {
    const { changeLanguage, isAuth } = this.props
    return (
      <HeaderWrapper>
        <div />
        <Logo src={logo} alt="iQuandexLogo" />
        <HeaderRight>
          <LanguagesButton changeLanguage={changeLanguage} />
          {isAuth && <Logout src={logoutImg} onClick={this.handleLogout} />}
        </HeaderRight>
      </HeaderWrapper>
    )
  }
}

Header.contextTypes = {
  intl: PropTypes.func,
}

const selectors = createStructuredSelector({
  isAuth: getUserAuthStatus,
})

const actions = {
  logout,
  changeLanguage,
}

export default connect(
  selectors,
  actions,
)(injectIntl(Header))
