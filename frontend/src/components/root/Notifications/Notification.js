import React from 'react'
import { func, string } from 'prop-types'
import { withSnackbar } from 'notistack'
import { injectIntl } from 'react-intl'

class Notification extends React.Component {
  static propTypes = {
    clearNotification: func,
    notificationMessage: string,
    notificationVariant: string,
  }
  static defaultProps = {
    clearNotification: () => {},
    notificationMessage: '',
    notificationVariant: 'default',
  }

  componentDidUpdate(prevProps) {
    const {
      intl,
      enqueueSnackbar,
      clearNotification,
      notificationVariant,
      notificationMessage,
    } = this.props
    if (
      prevProps.notificationMessage !== notificationMessage &&
      !!notificationMessage
    ) {
      let message = notificationMessage
      if (message.startsWith('ERROR.') || message.startsWith('SUCCESS.')) {
        message = `${intl.formatMessage({ id: `${message}` })}`
      }
      enqueueSnackbar(message, {
        id: 'notification',
        variant: notificationVariant,
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
        },
      })
      clearNotification()
    }
  }
  render() {
    return null
  }
}

export default withSnackbar(injectIntl(Notification))
