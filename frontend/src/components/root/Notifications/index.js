import { connect } from 'react-redux'
import Notification from './Notification'
import { clearNotification } from '../../../redux/actions'
import {
  getNotificationMessage,
  getNotificationVariant,
} from '../../../redux/selectors'

const actions = { clearNotification }
const mapStateToProps = state => ({
  notificationMessage: getNotificationMessage(state),
  notificationVariant: getNotificationVariant(state),
})

export default connect(
  mapStateToProps,
  actions,
)(Notification)
