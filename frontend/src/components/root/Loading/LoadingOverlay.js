import React from 'react'
import propTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { Loader } from './index'

import { getLoadingStatus } from '../../../redux/selectors'

const Overlay = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  background-image: radial-gradient(
    circle farthest-corner at center,
    #3a383599 0%,
    #000000 100%
  );
  z-index: 999;
`

const LoadingOverlay = ({ isVisible, selector }) =>
  isVisible ? (
    <Overlay data-selector={selector}>
      <Loader />
    </Overlay>
  ) : null

LoadingOverlay.propTypes = {
  isVisible: propTypes.bool.isRequired,
}

const mapStateToProps = state => ({
  isVisible: getLoadingStatus(state),
})

export default connect(mapStateToProps)(LoadingOverlay)
