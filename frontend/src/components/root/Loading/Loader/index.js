import React from 'react'

import './index.css'

const Loader = () => {
  return (
    <div className="loader">
      <div className="inner one" />
      <div className="inner two" />
      <div className="inner three" />
    </div>
  )
}

export { Loader }
