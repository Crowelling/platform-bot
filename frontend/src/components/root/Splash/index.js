import React from 'react'
import propTypes from 'prop-types'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'

const Splash = ({ isAuth }) =>
  isAuth ? <Redirect to="/main" /> : <Redirect to="/landing" />

Splash.propTypes = {
  isAuth: propTypes.bool.isRequired,
}

const mapStateToProps = state => ({
  isAuth: state.user.isAuth,
})

export default connect(
  mapStateToProps,
  null,
)(Splash)
