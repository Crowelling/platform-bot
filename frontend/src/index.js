import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import { SnackbarProvider } from 'notistack'

import './containers/AppContainer/index.css'
import './containers/AppContainer/normalize.css'
import { persistor, store } from './redux'
import App from './containers/AppContainer/App'
import { ThemeProvider } from './constants'
import { Locales, Notifications } from './components'
import { PersistGate } from 'redux-persist/integration/react'
import registerServiceWorker from './containers/AppContainer/registerServiceWorker'

const target = document.querySelector('#root')

render(
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      <Locales>
        <BrowserRouter basename={process.env.PUBLIC_URL}>
          <ThemeProvider>
            <SnackbarProvider maxSnack={1}>
              <>
                <App />
                <Notifications />
              </>
            </SnackbarProvider>
          </ThemeProvider>
        </BrowserRouter>
      </Locales>
    </PersistGate>
  </Provider>,
  target,
)

registerServiceWorker()
