import { createTypes } from 'redux-compose-reducer'

export const SIGN_IN_TYPES = createTypes('signIn', [
  'signInRequest',
  'signInSuccess',
  'signInFail',
])

/**
 * @param token -> token from user
 */

export const signIn = token => ({
  types: Object.values(SIGN_IN_TYPES),
  promise: api => api.user.signIn(token),
  payload: token,
})

export const SET_EXCHANGE_KEYS_TYPES = createTypes('setExchangeKeys', [
  'setExchangeKeysRequest',
  'setExchangeKeysSuccess',
  'setExchangeKeysFail',
])

/**
 * @param token -> token from user
 * @param data -> {
 *    exchange: "binance/bitmax",
 *    tariff: "string",
 *    apiKey: "string",
 *    secretKey: "string",
 *    }
 */

export const setExchangeKeys = (data, token) => ({
  types: Object.values(SET_EXCHANGE_KEYS_TYPES),
  promise: api => api.user.setExchangeKeys(data, token),
  payload: data,
})

export const USER_ACTION_TYPES = createTypes('userAction', ['logout'])

export const logout = () => ({
  type: USER_ACTION_TYPES.logout,
})
