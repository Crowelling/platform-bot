import { createSelector } from 'reselect'

const domainSelector = state => state.user

export const getUserAuthStatus = createSelector(
  domainSelector,
  user => user.isAuth,
)
