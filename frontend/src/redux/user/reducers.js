import { composeReducer } from 'redux-compose-reducer'
import ApiClient from '../../api/apiClient'
import update from 'update-js'

import { SIGN_IN_TYPES, USER_ACTION_TYPES } from './actions'

const initialState = {
  isAuth: false,
}

const signInSuccess = (state, { payload }) => {
  ApiClient.singleton.setToken(payload.token)

  return update(state, {
    isAuth: true,
  })
}

const logout = state => {
  ApiClient.singleton.setToken(null)
  window.localStorage.clear()

  return update(state, {
    isAuth: false,
  })
}

export default composeReducer({
  types: {
    ...SIGN_IN_TYPES,
    ...USER_ACTION_TYPES,
  },
  reducers: {
    logout,
    signInSuccess,
  },
  initialState,
})
