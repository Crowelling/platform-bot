import { createLogger } from 'redux-logger'
import apiMiddleware from './apiMiddleware'
import api from '../api'

const middlewares = [apiMiddleware(api)]
if (process.env.NODE_ENV === 'development') {
  middlewares.push(createLogger({ collapsed: true }))
}

export default middlewares
