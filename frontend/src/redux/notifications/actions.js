import { createTypes } from 'redux-compose-reducer'

export const NOTIFICATION_TYPES = createTypes('notification', ['clear'])

export const clearNotification = () => ({
  type: NOTIFICATION_TYPES.clear,
})
