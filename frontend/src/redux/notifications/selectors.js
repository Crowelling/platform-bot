export const getNotificationMessage = state => state.notifications.message
export const getNotificationVariant = state => state.notifications.variant
