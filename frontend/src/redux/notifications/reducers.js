import { composeReducer } from 'redux-compose-reducer'
import {
  SIGN_IN_TYPES,
  NOTIFICATION_TYPES,
  FETCH_ALL_DATA_TYPES,
  SET_EXCHANGE_KEYS_TYPES,
} from '../actions'

const initialState = {
  message: '',
  variant: 'error',
}

const updateMessage = (state, action) => {
  let variant
  let message

  if (action.data && action.data.success) {
    variant = 'success'
    message = action.data.success || 'SUCCESS.SUCCESS'
  } else if (action.error) {
    variant = 'error'
    message = action.error.message || 'ERROR.ERROR'
  }
  return {
    variant,
    message,
  }
}

const clear = { message: '' }

export default composeReducer({
  types: {
    ...NOTIFICATION_TYPES,
    ...SIGN_IN_TYPES,
    ...FETCH_ALL_DATA_TYPES,
    ...SET_EXCHANGE_KEYS_TYPES,

    signInFail: SIGN_IN_TYPES.signInFail,
    signInSuccess: SIGN_IN_TYPES.signInSuccess,

    setExchangeKeysFail: SET_EXCHANGE_KEYS_TYPES.setExchangeKeysFail,
    setExchangeKeysSuccess: SET_EXCHANGE_KEYS_TYPES.setExchangeKeysSuccess,

    fetchAllDataFail: FETCH_ALL_DATA_TYPES.fetchAllDataFail,
    fetchAllDataSuccess: FETCH_ALL_DATA_TYPES.fetchAllDataSuccess,
  },
  reducers: {
    clear,

    signInFail: updateMessage,
    signInSuccess: updateMessage,

    setExchangeKeysFail: updateMessage,
    setExchangeKeysSuccess: updateMessage,

    fetchAllDataFail: updateMessage,
    fetchAllDataSuccess: updateMessage,
  },
  initialState,
})
