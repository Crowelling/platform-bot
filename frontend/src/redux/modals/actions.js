import { createTypes } from 'redux-compose-reducer'

export const MODAL_TYPES = createTypes('modals', ['changeModalState'])

export const changeModalState = (name, newState) => getState => {
  const state =
    typeof newState === 'undefined'
      ? !getState(state => state.modals[name])
      : newState
  return {
    type: MODAL_TYPES.changeModalState,
    payload: { name, newState: state },
  }
}
