import { composeReducer } from 'redux-compose-reducer'
import update from 'update-js'

import { MODAL_TYPES } from '../actions'
import * as MODALS from '../../constants/modals'

const initialState = {
  [MODALS.SIGN_UP_MODAL]: false,
  [MODALS.SIGN_IN_MODAL]: false,
}

export const changeModalState = (state, { payload }) =>
  update.assign(state, {
    [payload.name]: payload.newState,
  })

export default composeReducer({
  types: {
    ...MODAL_TYPES,
  },
  reducers: {
    changeModalState,
  },
  initialState,
})
