import { createSelector } from 'reselect'

const domainSelector = state => state.modals

// isOpen: getModalState(state, { modalName: MODAL_NAME })

export const getModalState = createSelector(
  domainSelector,
  (_, { modalName }) => modalName,
  (modals, name) => modals[name],
)
