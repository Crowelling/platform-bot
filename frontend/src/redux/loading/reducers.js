import { composeReducer } from 'redux-compose-reducer'
import update from 'update-js'

import {
  LOADING_TYPES,
  SIGN_IN_TYPES,
  FETCH_ALL_DATA_TYPES,
  SET_EXCHANGE_KEYS_TYPES,
} from '../actions'

const initialState = {
  isLoading: false,
}

export const startLoading = state =>
  update.assign(state, {
    isLoading: true,
  })

export const endLoading = state =>
  update.assign(state, {
    isLoading: false,
  })

export default composeReducer({
  types: {
    ...LOADING_TYPES,
    ...SIGN_IN_TYPES,
    ...FETCH_ALL_DATA_TYPES,
    ...SET_EXCHANGE_KEYS_TYPES,
  },
  reducers: {
    startLoading,
    endLoading,

    signInRequest: startLoading,
    signInSuccess: endLoading,
    signInFail: endLoading,

    fetchAllDataRequest: startLoading,
    fetchAllDataSuccess: endLoading,
    fetchAllDataFail: endLoading,

    setExchangeKeysRequest: startLoading,
    setExchangeKeysSuccess: endLoading,
    setExchangeKeysFail: endLoading,
  },
  initialState,
})
