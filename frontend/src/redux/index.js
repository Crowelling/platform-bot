/* eslint-disable no-underscore-dangle */
/* eslint-disable no-undef */
/* global __DEV__ */
import { applyMiddleware, createStore, compose } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { createBrowserHistory } from 'history'
import middleware from './middleware'
import reducer from './reducers'

const enhancers = [applyMiddleware(...middleware)]

const isDEV = () => {
  try {
    return __DEV__
  } catch (e) {
    return !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
  }
}

const composeEnhancers =
  (isDEV() &&
    typeof window !== 'undefined' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose
/* eslint-enable no-undef */

const enhancer = composeEnhancers(...enhancers)

const persistConfig = {
  key: 'narflexNeuroRoot',
  storage,
  blacklist: ['library'],
}

const persistedReducer = persistReducer(persistConfig, reducer)

export const store = createStore(persistedReducer, {}, enhancer)
export const persistor = persistStore(store)

export const history = createBrowserHistory()

export default { persistor, store, history }
