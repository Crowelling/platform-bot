import { createTypes } from 'redux-compose-reducer'

export const FETCH_ALL_DATA_TYPES = createTypes('fetchAllDataTypes', [
  'fetchAllDataRequest',
  'fetchAllDataSuccess',
  'fetchAllDataFail',
])

/**
 * @param token -> token from user
 */

export const fetchAllData = token => ({
  types: Object.values(FETCH_ALL_DATA_TYPES),
  promise: api => api.main.fetchAllData(token),
})
