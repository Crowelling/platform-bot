import { createSelector } from 'reselect'

const domainSelector = state => state.main

export const getAllData = createSelector(
  domainSelector,
  main => main.data,
)

export const getUserInfo = createSelector(
  domainSelector,
  main => main.userInfo,
)

export const getTotalInfo = createSelector(
  domainSelector,
  main => main.totalInfo,
)

export const getTariffInfo = createSelector(
  domainSelector,
  main => {
    if (!main.tariffInfo) return []

    return main.tariffInfo.map(tariff => {
      if (tariff.status === 'active') {
        tariff.exchange.graphicData = {
          chart: {
            backgroundColor: {
              linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
              stops: [[0, '#232023'], [1, '#232023']],
            },
            plotBorderColor: '#606063',
          },

          title: {
            text: '',
          },

          xAxis: {
            categories: tariff.exchange.date, // вставка по дате
            labels: {
              style: {
                color: '#E0E0E3',
              },
            },
          },

          yAxis: {
            title: {
              text: '',
            },
            labels: {
              style: {
                color: '#E0E0E3',
              },
            },
          },

          plotOptions: {
            column: {
              borderRadiusTopLeft: 2,
              borderRadiusTopRight: 2,
            },
          },

          credits: {
            enabled: false,
          },

          legend: {
            itemStyle: {
              color: '#E0E0E3',
            },
            title: {
              style: {
                color: '#E0E0E3',
              },
            },
            itemHoverStyle: {
              color: '#ffc106',
            },
          },

          series: [
            {
              type: 'column',
              name: 'Покупка',
              legendColor: '#28a745',
              color: '#28a745',
              borderColor: '#232023',
              data: tariff.exchange.buys, // вставка по покупкам
              // data: [72,28,34,54,37,24,31,18,59,12,22,36,13,21,28,29,32,42,36,23,22,38], // тест по покупкам
            },
            {
              type: 'column',
              name: 'Продажа',
              legendColor: '#aa4f43',
              color: '#aa4f43',
              borderColor: '#232023',
              data: tariff.exchange.sells, // вставка по продажам
              // data: [82,18,24,44,47,44,41,38,29,42,22,36,43,31,18,29,32,32,46,53,52,48], // тест по продажам
            },
            {
              type: 'spline',
              name: 'Прибыль',
              legendColor: '#ffc106',
              color: '#ffc106',
              data: tariff.exchange.earns, // вставка по прибыли
              // data: [92,38,44,64,57,64,71,28,39,52,32,26,43,71,28,39,52,32,56,63,72,88], // тест по прибыли
              marker: {
                lineWidth: 2,
                lineColor: '#ffc106',
                fillColor: '#232023',
              },
            },
          ],
        }
      }

      return tariff
    })
  },
)
