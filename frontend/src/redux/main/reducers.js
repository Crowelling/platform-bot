import { composeReducer } from 'redux-compose-reducer'
import update from 'update-js'

import { mockData } from './mocks'
import { FETCH_ALL_DATA_TYPES, SET_EXCHANGE_KEYS_TYPES } from '../actions'

const initialState = {
  data: null,
  userInfo: mockData.userInfo,
  totalInfo: mockData.totalInfo,
  tariffInfo: mockData.tariffInfo,
}

const fetchAllDataSuccess = (state, { data }) => {
  return update(state, {
    data,
    userInfo: data.user,
    totalInfo: data.total,
    tariffInfo: data.robots,
  })
}

const setExchangeKeysSuccess = (state, { payload, data }) => {
  const { robotID } = payload
  let tariffInfo = [...state.tariffInfo]
  tariffInfo = tariffInfo.map(robot => {
    if (robot.id === robotID) {
      robot.status = 'wait'
      robot.deposit = data.deposit
    }
    return robot
  })

  let allData = { ...state.data }

  let index = allData.robots.indexOf(
    allData.robots.find(robot => robot.id === robotID),
  )

  allData.robots[index].status = 'wait'

  return update(state, {
    data: allData,
    tariffInfo,
  })
}

export default composeReducer({
  types: {
    ...FETCH_ALL_DATA_TYPES,
    ...SET_EXCHANGE_KEYS_TYPES,
  },
  reducers: {
    fetchAllDataSuccess,
    setExchangeKeysSuccess,
  },
  initialState,
})
