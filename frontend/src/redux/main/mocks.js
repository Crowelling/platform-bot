export const mockData = {
  userInfo: {
    name: '',
    surname: '',
    status: '',
    phone: '',
    email: '',
    token: '',
  },
  totalInfo: {
    totalSells: 0,
    totalBuys: 0,
    totalEarns: 0,
  },
  tariffInfo: [
    {
      name: 'Тариф «IQ Start Bot»',
      status: 'wait',
    },
    {
      name: 'Тариф «IQ Bot»',
      status: 'wait',
    },
    {
      name: 'Тариф «IQ Pro Bot»',
      status: 'wait',
    },
  ],
}
