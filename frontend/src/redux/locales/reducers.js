import { composeReducer } from 'redux-compose-reducer'

import { LOCALES_TYPES } from '../actions'

import update from 'update-js'
import {
  russianTranslations,
  englishTranslations,
} from '../../constants/translations'

const initialState = {
  languages: [
    {
      lang_key: 'ru',
      lang_label: 'Русский',
    },
    {
      lang_key: 'en',
      lang_label: 'English',
    },
  ],
  translations: russianTranslations,
}

export const changeLanguage = (state, { payload }) => {
  let translations
  if (payload === 'en') {
    translations = englishTranslations
  } else {
    translations = russianTranslations
  }

  return update.assign(state, {
    translations,
  })
}

export default composeReducer({
  types: {
    ...LOCALES_TYPES,
  },
  reducers: {
    changeLanguage,
  },
  initialState,
})
