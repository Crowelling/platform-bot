import { combineReducers } from 'redux'
import { routerReducer as routing } from 'react-router-redux'

import user from '../user/reducers'
import main from '../main/reducers'
import modals from '../modals/reducers'
import loading from '../loading/reducers'
import locales from '../locales/reducers'
import notifications from '../notifications/reducers'

export default combineReducers({
  routing,

  user,
  main,
  modals,
  loading,
  locales,
  notifications,
})
