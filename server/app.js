require('dotenv').config()

import { registerAllRoutes } from './routes'

import setupKOA from './middlewares/koa'
import connectMongo from './middlewares/mongo'

const Koa = require('koa')
const app = new Koa()

setupKOA(app)
connectMongo()
registerAllRoutes(app)

const port = process.env.APP_PORT || 9970
app.listen(port)
console.log(`Application started on port ${port}`)
