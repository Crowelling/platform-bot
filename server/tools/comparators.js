import _ from 'lodash'
import { mockExchangeIDs } from '../services/mocks/mocks_processUserData'

export const compareUserData = (mongoUser, user) => {
  let compareMongo = JSON.parse(JSON.stringify(mongoUser));
  let compareUser = JSON.parse(JSON.stringify(user));
  ['_id', '__v', 'robots', 'token'].forEach(
    el => delete compareMongo[el]
  )
  delete compareUser['tariffs']
  delete compareUser['token']

  return _.isEqual(compareMongo, compareUser)
}

export const isAvailableRobot = robots => {
  const activeRobots = robots.filter(robot => robot.status === 'active')
  return activeRobots.length < mockExchangeIDs.length
}
