import Exception from '../services/Exception'

const defaultParamsBuilder = () => ({})
const defaultContextBuilder = ctx => ctx.state.user || {}

export async function runService(ServiceClass, { context = {}, params = {} }) {
  try {
    const result = await new ServiceClass({
      context,
    }).run(params)
    return result
  } catch (error) {
    throw error
  }
}

export async function renderPromiseAsJson(ctx, promise) {
  try {
    const result = await promise
    ctx.status = 200
    ctx.body = {
      ...result,
    }
  } catch (error) {
    console.log('ERROR -> ', error)
    if (error instanceof Exception) {
      ctx.status = error.message ? error.code : 400
      ctx.body = {
        // TODO: figure out why we can't pass fields other than 'message'
        message: error.message ? error.toRegular() : error.toHash(),
      }
    } else {
      ctx.status = 500
      ctx.body = {
        // TODO: figure out why we can't pass fields other than 'message'
        message: {
          code: 'SERVER_ERROR',
          message: 'Please, contact your system administrator!',
        },
      }
    }
  }
}

export function actionRunner(
  serviceClass,
  paramsBuilder = defaultParamsBuilder,
  contexBuilder = defaultContextBuilder
) {
  return async function serviceRunner(ctx) {
    const resultPromise = runService(serviceClass, {
      params: paramsBuilder(ctx),
      context: contexBuilder(ctx),
    })

    return renderPromiseAsJson(ctx, resultPromise)
  }
}
