import log from 'koa-logger'
import cors from '@koa/cors'
import serve from 'koa-static'
import mount from 'koa-mount'
import cookie from 'koa-cookie'

const koaBody = require('koa-body')
const koaQueryString = require('koa-qs')

const headers = [
  'authorization',
  'accept',
  'application-id',
  'application-language',
  'content-type',
  'token',
  'access-control-allow-origin',
]
const setupKOA = app => {
  app.use(mount('/', serve('./public')))
  app.use(mount('/static', serve('./static')))

  if (process.env.APP_ENV !== 'production') {
    app.use(log())
  }

  app.use(
    cors({
      exposeHeaders: headers,
      allowHeaders: headers,
      credentials: true,
      origin: '*',
      methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT, PATCH, DELETE',
      'Access-Control-Allow-Headers': 'X-Requested-With,content-type',
      'Access-Control-Allow-Credentials': true,
    })
  )

  koaQueryString(app, 'extended')
  app.use(
    koaBody({
      multipart: true,
    })
  )
  app.use(cookie())
}

export default setupKOA
