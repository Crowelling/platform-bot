import mongoose from 'mongoose'
const connectMongo = () => {
  const uri = 'mongodb://127.0.0.1:27017/quantex_dev'

  mongoose
    .connect(uri, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
    })
    .then(() => console.log('Mongoose connected!'))
    .catch(e => console.log(e))
}
export default connectMongo
