import { Schema, model } from 'mongoose'

const UsersSchema = new Schema({
  name: { type: String, required: true },
  surname: { type: String, required: true },
  status: { type: String },
  phone: { type: String },
  email: { type: String, required: true },
  token: { type: String, required: true },
  robots: [
    {
      id: String,
      name: String,
      deposit: {
        min: Number,
        max: Number
      },
      finishTo: Number,
      status: { type: String, default: 'wait' },
      exchange: {
        name: String,
        userWalletID: String,
        apiKey: { type: String, default: null },
        secretKey: { type: String, default: null },
        dayStart: { type: Date, default: null }, // save Date in minutes/seconds
        deposit: { type: Number, default: null }, // we cant start robot if deposit > tariff.deposit  || deposit < tariff.deposit
      },
    },
  ],
})

module.exports = model('Users', UsersSchema)
