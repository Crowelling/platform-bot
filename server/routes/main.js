import Router from 'koa-router'
import mainControllers from '../contollers/main'

const router = new Router()

router.get('/main', mainControllers.getMainInfo)
router.get('/signin', mainControllers.signIn)
router.post('/setExchangeKeys', mainControllers.exchangeKeys)

export default router
