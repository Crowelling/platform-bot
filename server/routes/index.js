import mainRouter from './main'

export function registerAllRoutes(app) {
  app.use(mainRouter.routes())
}
