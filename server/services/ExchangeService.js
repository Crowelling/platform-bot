import ccxt from 'ccxt'
import moment from 'moment'
import Exception from './Exception'
import { mathRound } from '../tools'

class ExchangeService {
  static async checkApiKeys({ apiKey, secretKey, exchange }) {
    const exchangeClass = ccxt[exchange],
      exchangeSession = new exchangeClass({
        apiKey,
        secret: secretKey,
        timeout: 30000,
        enableRateLimit: true,
      })
    let userWalletID
    try {
      if (exchange === 'binance') {
        let resp = await exchangeSession.fetchDepositAddress('BTC')
        userWalletID = resp.address
      } else if (exchange === 'bitmex') {
        let resp = await exchangeSession.fetchLedger()
        userWalletID = resp[0].account
      }
      return userWalletID
    } catch (err) {
      console.log(err)
      // todo: add diff catching for AuthenticationError and exchange doesn't answer
      throw new Exception({
        message: 'ERROR.KEYS_NOT_EXIST',
        code: 404,
        fields: { keys: 'wrong' },
      })
    }
  }

  static async getBalance({ exchange, apiKey, secretKey }) {
    const exchangeClass = ccxt[exchange],
      exchangeSession = new exchangeClass({
        apiKey: apiKey,
        secret: secretKey,
        timeout: 30000,
        enableRateLimit: true,
      })

    const balance = await exchangeSession.fetchBalance()
    if (exchange === 'binance') {
      return balance['USDT'].free
    } else if (exchange === 'bitmex') {
      return balance.free.USD
    }
    // todo: check what balance is needed, total (free + used) or just free
  }

  static async getAllTrades({
    id,
    tariffName,
    status,
    exchange,
    apiKey,
    secretKey,
    startFrom,
    deposit,
  }) {
    let baseCurrency, market
    if (exchange === 'binance') {
      baseCurrency = 'USDT'
      market = 'BTC/USDT'
    } else if (exchange === 'bitmex') {
      baseCurrency = 'USD'
      market = 'BTC/USD'
    }
    const exchangeClass = ccxt[exchange],
      exchangeSession = new exchangeClass({
        apiKey: apiKey,
        secret: secretKey,
        timeout: 30000,
        enableRateLimit: true,
      })

    //todo pass props limit and pagination
    const trades = await exchangeSession.fetchMyTrades(
      market,
      exchange == 'binance' ? moment(startFrom).format('x') : startFrom
    )
    const totalData = trades.reduce((acc, curr) => {
      curr.datetime = moment(curr.datetime).format('DD.MM.YYYY')
      let pres = acc.filter(el => el.date === curr.datetime)
      if (pres.length === 0) {
        acc.push({
          date: curr.datetime,
          earnedUSD: curr.fee.currency === baseCurrency ? curr.fee.cost : 0,
          sells: curr.side === 'sell' ? 1 : 0,
          buys: curr.side === 'buy' ? 1 : 0,
        })
      } else {
        if (curr.fee.currency === baseCurrency) {
          acc[acc.indexOf(pres[0])].earnedUSD += curr.fee.cost
        }
        if (curr.side === 'sell') {
          acc[acc.indexOf(pres[0])].sells += 1
        }
        if (curr.side === 'buy') {
          acc[acc.indexOf(pres[0])].buys += 1
        }
      }
      return acc
    }, [])

    const date = totalData.map(el => el.date)
    const sells = totalData.map(el => el.sells)
    const buys = totalData.map(el => el.buys)
    const earns = totalData.map(el => mathRound(el.earnedUSD))
    const feeTotalUSD = earns.reduce((acc, curr) => (acc += mathRound(curr)), 0)
    const totalCountBuys = trades.filter(el => el.side === 'buy').length
    const totalCountSells = trades.filter(el => el.side === 'sell').length
    const feeHalfMonthUSD =
      earns.length >= 10
        ? mathRound(
            earns.slice(-10, earns.length).reduce((acc, curr) => (acc += curr), 0)
          )
        : mathRound(feeTotalUSD)
    const feeTotalPercent = mathRound(feeTotalUSD / deposit * 100)
    const feeHalfMonthPercent = mathRound(feeHalfMonthUSD / deposit * 100)

    return {
      id,
      name: tariffName,
      status,
      exchange: {
        name: exchange,
        dayStart: moment(startFrom).format('DD.MM.YYYY'),
        deposit,
        feeTotalPercent: feeTotalPercent === Infinity ? 0 : feeTotalPercent,
        feeHalfMonthPercent: feeHalfMonthPercent === Infinity ? 0: feeHalfMonthPercent,
        feeTotalUSD,
        feeHalfMonthUSD,
        totalCountSells,
        totalCountBuys,
        date,
        sells,
        buys,
        earns,
      },
    }
  }

  static async getAggregatedRobotsStat(array) {
    return {
      totalSells: array.reduce(
        (acc, curr) => (acc += curr.exchange.totalCountSells),
        0
      ),
      totalBuys: array.reduce(
        (acc, curr) => (acc += curr.exchange.totalCountBuys),
        0
      ),
      feeTotalUSD: array.reduce(
        (acc, curr) => (acc += mathRound(curr.exchange.feeTotalUSD)),
        0
      ),
      feeHalfMonthUSD: array.reduce(
        (acc, curr) => (acc += mathRound(curr.exchange.feeHalfMonthUSD)),
        0
      ),
    }
  }
}

export default ExchangeService
