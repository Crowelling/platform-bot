export const mockUsers = [
  {
    userInfo: {
      name: 'John',
      surname: 'Kovalskiy',
      status: 'User',
      phone: '+71 (321) 432-43-321',
      email: 'john.kovalskiy@gmail.com',
      token: '6NJu%yBU6o9gty#789nkb&9090',
      tariffs: [
        {
          id: 'tatatananana',
          name: 'IQ Start Bot',
          deposit: {
            min: 0,
            max: 250,
          },
          finishTo: 1.45,
        },
        {
          id: 'papapazazaza',
          name: 'IQ Pro Bot',
          deposit: {
            min: 100,
            max: 450,
          },
          finishTo: 1.55,
        },
      ],
    },
  },
  {
    userInfo: {
      name: 'Evie',
      surname: 'Monreal',
      status: 'User',
      phone: '+81 (321) 000-11-321',
      email: 'evie.monreal@gmail.com',
      token: 'rrtttNJu%yBU6o9gty34ggnkb&9090',
      tariffs: [
        {
          id: "sasasararara",
          name: 'IQ Start Bot',
          deposit: {
            min: 100,
            max: 250,
          },
          finishTo: 1.45,
        }
      ],
    },
  },
  {
    userInfo: {
      name: 'Emilie',
      surname: 'Smith',
      status: 'User',
      phone: '+71 (321) 999-99-999',
      email: 'emilie.smith@hotmail.com',
      token: '2NJu%yemiliegty#789nkb&9090',
      tariffs: [],
    },
  },
]
