// HOME server (HS) -> our server/backend/data
// WALLET server (WS) -> Renat's server/backend/data
// ROBOT server (RS) -> robot api server/backend/data

const createData = isDate => {
  let result = []
  for (let i = 1; i < 10; i++) {
    if (isDate) {
      result.push(`${i}.02`)
    } else {
      result.push(Math.floor(Math.random() * 20))
    }
  }

  return result
}

export const data = {
  user: {
    name: 'John',
    surname: 'Kovalskiy',
    status: 'User', // скорее всего будет изменяться в зависимости от реферальной программы
    phone: '+71 (321) 432-43-321',
    email: 'john.kovalskiy@gmail.com',
    token: '6NJu%y*******nkb&9090',
  },
  total: {
    totalSells: 324, // все продажи со всех роботов
    totalBuys: 432, // все покупки со всех роботов
    feeTotalUSD: 2014.42, // получка за последние 2 недели
    feeHalfMonthUSD: 7312.22, // получка за всё время
  },
  robots: [
    {
      name: 'IQ Bot',
      status: 'active',
      exchange: {
        name: 'Binance',
        dayStart: '02.01.2020',
        deposit: 1500,
        feeTotalUSD: 533,
        feeHalfMonthUSD: 321,
        totalCountSells: 65,
        totalCountBuys: 71,
        date: createData(true),
        sells: createData(),
        buys: createData(),
        earns: createData(),
      },
    },
    {
      name: 'IQ Bot',
      status: 'active',
      exchange: {
        name: 'Bitmex',
        dayStart: '12.01.2020',
        deposit: 1000,
        feeTotalUSD: 876,
        feeHalfMonthUSD: 231,
        totalCountSells: 76,
        totalCountBuys: 34,
        date: createData(true),
        sells: createData(),
        buys: createData(),
        earns: createData(),
      },
    },
    {
      name: 'IQ Pro Bot',
      status: 'active',
      exchange: {
        name: 'Binance',
        dayStart: '20.01.2020',
        deposit: 2000,
        feeTotalUSD: 654,
        feeHalfMonthUSD: 123,
        totalCountSells: 45,
        totalCountBuys: 32,
        date: createData(true),
        sells: createData(),
        buys: createData(),
        earns: createData(),
      },
    },
    {
      name: 'IQ Start Bot',
      status: 'wait',
    },
    {
      name: 'IQ Super Bot',
      status: 'wait',
    },
  ],
}
