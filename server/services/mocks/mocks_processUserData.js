export const mockTariffs = [
  'Тариф «IQ Start Bot»',
  'Тариф «IQ Bot»',
  'Тариф «IQ Pro Bot»',
  'Тариф «IQ Super Bot»',
  'Тариф «IQuandex Bot»',
]

export const mockExchangeIDs = [
  'Binance',
  'Bitmex',
]
