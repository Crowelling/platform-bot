import Exception from './Exception'
import Users from '../models/Users'
import { compareUserData } from '../tools'
import { mockUsers } from './mocks'

class UserService {
  static async validateAuthToken(token) {
    if (!token) {
      throw new Exception({
        code: 'MISSING_ARGUMENTS',
        fields: { token: 'required' },
      })
    }
    // request to Renat service
    // response from Renat service
    const respFromRS = mockUsers.find(user => user.userInfo.token === token)
    // add verification for the response: check sign in headers (use jwt)

    if (!respFromRS) {
      return {
        message: 'ERROR.NO_SUCH_USER',
        code: 404,
        fields: { token: 'wrong' },
      }
    }

    return respFromRS.userInfo
  }

  static async checkUserInDB(user) {
    if (!user) {
      throw new Exception({
        code: 'MISSING_ARGUMENTS',
        fields: { user: 'required' },
      })
    }

    const mongoUser = await Users.findOne({ token: user.token }).lean()

    if (!mongoUser) {
      await Users.create({
        email: user.email,
        name: user.name,
        surname: user.surname,
        status: user.status,
        phone: user.phone,
        token: user.token,
        robots: user.tariffs.reduce((acc, curr) => {
          acc.push({
            id: curr.id,
            name: curr.name,
            deposit: {
              min: curr.deposit.min,
              max: curr.deposit.max,
            },
            finishTo: curr.finishTo,
            status: 'inactive',
          })
          return acc
        }, []),
      })
    } else {
      const isEqual = compareUserData(mongoUser, user)

      if (!isEqual) {
        const robotsDB = mongoUser.robots.map(robot => robot.id)
        let newRobots = user.tariffs.filter(
          tariff => !robotsDB.includes(tariff.id)
        )
        await Users.updateOne(
          { token: user.token },
          {
            $set: {
              email: user.email,
              name: user.name,
              surname: user.surname,
              status: user.status,
              phone: user.phone,
            },
            $push: {
              robots: newRobots.map(robot => {
                return {
                  id: robot.id,
                  name: robot.name,
                  deposit: {
                    min: robot.deposit.min,
                    max: robot.deposit.max,
                  },
                  finishTo: robot.finishTo,
                  status: 'inactive',
                }
              }),
            },
          }
        )
      }
    }
    return await Users.findOne({ token: user.token }).lean()
  }

  static async checkActiveRobots(user) {
    return user.robots.filter(robot => robot.status === 'active')
  }

  static async checkWaitRobots(user) {
    return user.robots
      .filter(robot => robot.status === 'inactive' || robot.status === 'wait')
      .reduce((acc, curr) => {
        acc.push({
          id: curr.id,
          name: curr.name,
          status: curr.status,
          deposit: {
            min: curr.deposit.min,
            max: curr.deposit.max,
          },
          finishTo: curr.finishTo,
        })
        return acc
      }, [])
  }
}

export default UserService
