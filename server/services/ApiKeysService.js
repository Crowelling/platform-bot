import Users from '../models/Users'
import ExchangeService from './ExchangeService'
import Exception from './Exception'

class ApiKeysService {
  static async createApiKeys(data) {
    const { token, robotID, apiKey, secretKey, exchange, userWalletID } = data

    const mongoUser = await Users.findOne({ token }).lean()
    // check if userWalletID of new keys isn't the same, that is already used by working bot
    if (
      mongoUser.robots.filter(
        robot => robot.exchange.userWalletID === userWalletID
      ).length
    ) {
      throw new Exception({
        message: 'ERROR.SAME_EXCHANGE_ACCOUNT',
        code: 404,
        fields: { keys: 'wrong' },
      })
    }

    if (
      mongoUser.robots.filter(robot => robot.id.toString() === robotID.toString())
        .length === 0
    ) {
      throw new Exception({
        message: 'ERROR.WRONG_ROBOT_ID',
        code: 404,
        fields: { keys: 'wrong' },
      })
    }

    // check that account's balance is correct
    let tariffDepositScope = mongoUser.robots.filter(
      robot => robot.id.toString() === robotID.toString()
    )[0].deposit
    let balance = await ExchangeService.getBalance({
      exchange,
      apiKey,
      secretKey,
    })
    if (balance === undefined) {
      throw new Exception({
        message: 'ERROR.INVALID_BALANCE_CURRENCY',
        code: 404,
        fields: { keys: 'wrong' },
      })
    }

    if (balance < tariffDepositScope.min || balance > tariffDepositScope.max) {
      throw new Exception({
        message: 'ERROR.INVALID_BALANCE',
        code: 404,
        fields: { keys: 'wrong' },
      })
    }

    // save keys
    await Users.updateOne(
      { token: token, 'robots.id': robotID },
      {
        $set: {
          'robots.$.status': 'wait',
          'robots.$.exchange': {
            name: exchange,
            userWalletID: userWalletID,
            apiKey: apiKey,
            secretKey: secretKey,
            dayStart: new Date(),
            deposit: balance,
          },
        },
      }
    )
    return { success: 'SUCCESS.ADD_KEYS', balance }
  }
}

export default ApiKeysService
