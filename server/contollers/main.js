import { actionRunner } from '../tools/actionRunner'
import MainAction from '../actions/MainAction'
import ExchangeKeys from '../actions/ExchangeKeys'

export default {
  getMainInfo: actionRunner(MainAction, ctx => ({ token: ctx.request.header.token })),
  signIn: actionRunner(MainAction, ctx => ({ token: ctx.request.header.token })),
  exchangeKeys: actionRunner(ExchangeKeys, ctx => ({
    token: ctx.request.header.token,
    ...ctx.request.body,
  })),
}
