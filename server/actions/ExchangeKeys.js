import Base from './Base'
import UserService from '../services/UserService'
import ExchangeService from '../services/ExchangeService'
import ApiKeysService from '../services/ApiKeysService'
import Exception from '../services/Exception'

class ExchangeKeys extends Base {
  static get validationRules() {
    return {
      token: ['required', 'string'],
      exchange: ['required', 'string'],
      robotID: ['required', 'string'],
      apiKey: ['required', 'string'],
      secretKey: ['required', 'string'],
    }
  }

  async execute({ token, exchange, robotID, apiKey, secretKey }) {
    const userWalletID = await ExchangeService.checkApiKeys({apiKey, secretKey, exchange})

    return await ApiKeysService.createApiKeys({
      token, robotID, apiKey, secretKey, exchange, userWalletID
    })
  }
}

export default ExchangeKeys
