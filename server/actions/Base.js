import LIVR from 'livr'
import extraRules from 'livr-extra-rules'
import Exception from '../services/Exception'

LIVR.Validator.registerDefaultRules(extraRules)

export default class Base {
  constructor(args) {
    if (!args.context) throw new Error('CONTEXT_REQUIRED')
    this.context = args.context
  }

  async run(params) {
    const cleanParams = await this.validate(params)
    return this.execute(cleanParams)
  }

  validate(data) {
    // Cache validator
    const validator =
      this.constructor.validator ||
      new LIVR.Validator(this.constructor.validationRules).prepare()
    this.constructor.validator = validator

    return this._doValidationWithValidator(data, validator)
  }

  _doValidationWithValidator(data, validator) {
    const result = validator.validate(data)

    if (!result) {
      const exception = new Exception({
        code: 'FORMAT_ERROR',
        fields: validator.getErrors(),
      })
      return Promise.reject(exception)
    }

    return Promise.resolve(result)
  }
}
