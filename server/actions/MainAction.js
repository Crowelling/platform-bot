import moment from 'moment'
import Base from './Base'
import { UserService } from '../services'
import { data } from '../services/mocks'
import { ExchangeService } from '../services'
import Exception from '../services/Exception'

class MainAction extends Base {
  static get validationRules() {
    return {
      token: ['required', 'string'],
    }
  }

  async execute({ token }) {
    // check if user exist on Renat's server
    const user = await UserService.validateAuthToken(token)
    if (user.code) throw new Exception(user)

    // check if user exist in mongo, if no, create one, if yes, update
    const mongoUser = await UserService.checkUserInDB(user)

    // check for robots with status 'active'
    // todo: later need to add separate collection with data of finished robots
    const activeRobots = await UserService.checkActiveRobots(mongoUser)
    const waitRobots = await UserService.checkWaitRobots(mongoUser)

    let activeRobotsStatistic = []
    let total = {
      totalSells: 0,
      totalBuys: 0,
      feeTotalUSD: 0,
      feeHalfMonthUSD: 0,
    }
    if (activeRobots.length !== 0) {
      activeRobotsStatistic = await Promise.all(
        activeRobots.map(robot => {
          return ExchangeService.getAllTrades({
            id: robot.id,
            tariffName: robot.name,
            status: robot.status,
            exchange: robot.exchange.name,
            apiKey: robot.exchange.apiKey,
            secretKey: robot.exchange.secretKey,
            startFrom: robot.exchange.dayStart,
            deposit: robot.exchange.deposit,
          })
        })
      )

      total = await ExchangeService.getAggregatedRobotsStat(activeRobotsStatistic)
    }

    const robots = [...activeRobotsStatistic, ...waitRobots]

    return {
      success: 'SUCCESS.SUCCESS',
      user: {
        name: mongoUser.name,
        surname: mongoUser.surname,
        status: mongoUser.status,
        phone: mongoUser.phone,
        email: mongoUser.email,
      },
      total,
      robots,
    }
  }
}

export default MainAction
