# Attention! This readme only for frontend and server. For cexminer you can see readme in `~/cexminer`

# How to run separatelly frontend and server - see scripts in `package.json`. Don't forget to install node_modules for each of them.

# How to run all in docker:
1. be sure that docker and docker-compose (on macOS Docker desktop) are installed on your machine:
    `docker ps`
2. `docker-compose up --build`
